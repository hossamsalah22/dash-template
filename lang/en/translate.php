<?php
return [
    "something_went_wrong" => "Something went wrong",
    "password_changed_successfully" => "Password changed successfully",
    "profile_not_completed" => "Profile not completed",
    "invalid_password" => "Invalid password",
    "already_completed" => "Profile already completed",
    "activate" => "Activate",
    "deactivate" => "Deactivate",


    "notes" => "Notes",

    "user_not_found" => "User not found",
    "logged_in_successfully" => "Logged in successfully",
    "logged_out_successfully" => "Logged out successfully",
    "registered_successfully" => "Registered successfully",
    "data_retrieved_successfully" => "Data retrieved successfully",
    "invalid_verification_code" => "Invalid verification code",
    "expired_verification_code" => "Expired verification code",
    "email_verified_successfully" => "Email verified successfully",
    "verification_code_sent_successfully" => "Verification code sent successfully",
    "password_reset_successfully" => "Password reset successfully",
    "visit" => "Visit",
    "link" => "Link",
    "read_at" => "Read At",
    "read_by" => "Read By",


    "notifications" => 'Notifications',
    "nafath_not_verified" => "Your account is not verified by Nafath yet",

    "locale" => "locale",
    "country" => "country",
    "gender" => "gender",
    "bank_number" => "bank number",
    "education" => "education",
    "birth_date" => "birth date",
    "national_id" => "national id",
    "manask_knowledge" => "manask knowledge",
    "ummra_performed" => "ummra performed",
    "is_residental" => "is residental",
    "nafath_verified" => "nafath verified",
    "user_id" => "User",
    "message" => "Message",
    "subject_title" => "Subject",
    "mark_as_read" => "Mark as read",
    "mark_as_unread" => "Mark as unread",
    "mark_as_replied" => "Mark as replied",
    "mark_as_unreplied" => "Mark as un replied",
    "contacts" => "Messages",
    "contact" => "Message",
    "is_read" => "is read",
    "is_replied" => "is replied",
    "message_received" => "Your message has been received successfully",
    "page" => "Page",
    "pages" => "Pages",
    "show" => "Show",
    "account_disabled" => "Your account is disabled at this moment",
    "cant_toggle_admin" => "You can't toggle this admin",
    "cant_toggle_yourself" => "You can't toggle yourself",
    "type" => "Type",
    "value" => "Value",
    'sliders' => 'Ads Slider',
    'slider' => 'Ad Slider',
    "active" => "Active",
    "email_already_verified" => "Email already verified",

    "start_at" => "Start Date",
    "expires_at" => "Expires Date",

    "delete_confirmation" => "Are you sure you want permanently delete this item ?",
    "deleted_successfully" => "Deleted Successfully",
    "updated_successfully" => "Updated Successfully",
    'website_name' => 'Website Name',
    'created_successfully' => 'Created Successfully',
    'edited_successfully' => 'Edited Successfully',
    'added_successfully' => 'Added Successfully',
    'retrieved_successfully' => 'Retrieved Successfully',
    'app_logo' => 'App Logo',
    'dashboard_logo' => 'Dashboard Logo',
    "retrieve_successfully" => "Retrieved Successfully",
    'drop_files_here_or_click_to_upload' => 'Drop files here or click to upload',
    "admins" => "Admins",
    "admin" => "Admin",
    "logout" => "Logout",
    'name' => 'Name',
    'description' => 'Description',
    'status' => 'Status',
    'action' => 'Action',
    'actions' => 'Actions',
    'image' => 'Image',
    'created_at' => 'Created At',
    "update"=>"Update",
    "no"=>"No",
    "yes"=>"Yes",
    'updated_at' => 'Updated At',
    'deleted_at' => 'Deleted At',
    'close' => 'Close',
    'home' => 'Home',
    'create' => 'Create',
    'index' => 'Index',
    'edit' => 'Edit',
    'delete' => 'Delete',
    "password" => "Password",
    "password_confirmation" => "Password Confirmation",
    'is_active' => 'Active',
    "users" => 'Users',
    'user' => 'User',
    'app_name' => 'App Name',
    'logo' => 'Logo',
    'email' => 'Email',
    'instagram_link' => 'Instagram Link',
    'facebook_link' => 'Facebook Link',
    'twitter_link' => 'Twitter Link',
    'snapchat_link' => 'Snapchat Link',
    'phone' => ' Phone',
    "app_links" => "App Links",
    'select_type' => 'Select Type',
    "introduction" => "Introduction",
    'technical_supports' => 'Technical Supports',
    'technical_support' => 'Technical Support',
    'subject' => 'Subject',
    'subjects' => 'Subjects',
    'marital_status' => 'Marital Status',
    'pending' => 'Pending',
    'validation_error' => 'Validation Error',
    'cancelled' => 'Cancelled',
    'validation_errors' => 'Validation Errors',
    'order_log' => 'Order Log',
    'already_checked' => 'Already Checked',
    'developed_by' => 'Developed By',
    'all_rights_reserved' => 'All Rights Reserved',
    "social_links" => "Social Links",
    "social_link" => "Social Link",
    "address" => "Address",
    "phone_1" => "Phone 1",
    "phone_2" => "Phone 2",
    "google_map_link" => "Google Map Link",
    "contact_us" => "Contact Us",
    "about_us" => "About Us",
    "general" => "General",
    "about_us_intro" => "About Us Intro",
    "about_us_image" => "About Us Image",
    "about_us_title" => "About Us Title",
    "about_us_description" => "About Us Description",
    "save" => "Save",
    "settings" => "Settings",
    "whatsapp_link" => "Whatsapp Link",
    "tiktok_link" => "Tiktok Link",
    "linkedin_link" => "Linkedin Link",
    "youtube_link" => "Youtube Link",
    "site_title" => "Site Title",
    "site_description" => "Site Description",
    "work_hours" => "Work Hours",
    "footer_paragraph" => "Footer Paragraph",
    "site_logo" => "Site Logo",
    "favicon" => "Favicon",
    "success" => "Success",

    'ar' => [
        'ar' => 'Arabic',
        'name' => 'Name in Arabic',
        'title' => 'Title in Arabic',
        'content' => 'Content in Arabic',
        'description' => 'Description in Arabic',
        'phone' => 'Phone',
        'address' => 'Address In Arabic',
        'email' => 'Email',
        'introduction' => 'Introduction in Arabic',
        'whatsapp_number' => 'Whatsapp',
        'youtube_link' => 'Youtube Link',
        'instagram_link' => 'Instagram Link',
        'facebook_link' => 'Facebook Link',
        'meta_title' => 'Meta Title in Arabic',
        'meta_description' => 'Meta Description in Arabic',
        'key' => 'Key in Arabic',
        'value' => 'Value in Arabic',
        'image' => 'Image viewed in Arabic',
        'company_name' => 'Company Name in Arabic',
        'note' => 'Note in Arabic',
        'short_description' => 'Short Description in Arabic',
        'author' => 'Author in Arabic',

    ],

    'en' => [

        'en' => 'English',
        'name' => 'Name in English',
        'title' => 'Title in English',
        'content' => 'Content in English',
        'description' => 'Description in English',
        'phone' => 'Phone',
        'address' => 'Address',
        'email' => 'Email',
        'introduction' => 'Introduction',
        'whatsapp_number' => 'whatsapp Number',
        'youtube_link' => 'Youtube Link',
        'instagram_link' => 'Instagram Link',
        'facebook_link' => 'Facebook Link',
        'meta_title' => 'Meta Title in English',
        'key' => 'Key in English',
        'value' => 'Value in English',
        'image' => 'Image viewed in English',
        'company_name' => 'Company Name in English',
        'note' => 'Note in English',
        'short_description' => 'Short Description in English',

    ],
];
