<?php

return [
    'new_message' => "New Message",
    'new_contact_message_from' => "New Contact Us Message Received from :name",
    'new_order' => "New Order",
    'new_order_from' => "New Order From :name",
    'your_umrah_request_has_been_assigned_to' => "Your Umrah Request number :number has been assigned to :name",
    'umrah_request_assigned' => "Umrah Request is Assigned",
    'you_have_new_umrah_request' => "You have new Umrah Request number :number",
    'umrah_request_accepted' => "Umrah Request is Accepted",
    'umrah_request_completed' => 'Umrah Request is Completed',
    'your_umrah_has_been_completed' =>'Your Umrah number :number has been completed by :name',
    'umrah_request_started' => 'Umrah Request is Started',
    'your_umrah_started_now' => 'Your Umrah number :number has been started by :name',
    'mansk_checked_for_request' => "Mansk :name has been checked for request number :number",
    'new_mansk_checked' => "New Mansk Checked",
    'order_number_status_updated_to' => "Order number :number status updated to :status",
    'order_status_updated' => 'Order Status Updated',
];
