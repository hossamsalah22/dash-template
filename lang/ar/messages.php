<?php
return [
	'done-remove' => 'تم الحذف بنجاح',
	'done-edit'	=>	'تم التعديل بنجاح',
	'done-create'	=>	'تم انشاء عنصر بنجاح',
	'done-create-app'	=>	'تم أضافة طلب تقديم جديد .',
	'done-create-inquiry'	=>	'تم ارسال الاستفسار الخاص بك .',
	'file-not-exist'	=>	'هذا الملف غير موجود بالنظام',
	'city_name_is_required'	=>	'اسم المدينه مطلوب',
    'validation_error'=>'خطأ في التحقق من البيانات',


];
?>
