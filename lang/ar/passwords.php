<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'يجب ان لا تقل كلمة المرور عن 6 حروف .',
    'reset' => 'تم اعاده تغير كلمة المرور .',
    'sent' => 'تم ارسال ايميل به رابط اعادة تعيين كلمة المرور .',
    'token' => 'رمز إعادة تعيين كلمة المرور هذا غير صالح.',
    'user' => "لم يتم الوصل الى البريد الالكتروني .",
    'throttled' => 'حاول مروة اخرى بعد مرور 10 ثواني .',

];
