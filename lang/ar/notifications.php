<?php

return [
    'new_message' => 'رسالة جديدة',
    'new_contact_message_from' => 'تم استلام رسالة تواصل جديدة من :name',
    'new_order' => 'طلب جديد',
    'new_order_from' => 'طلب جديد من :name',
    'new_umrah_request' => 'طلب عمرة جديد',
    'your_umrah_request_has_been_assigned_to'=>"تم تعيين طلب العمرة رقم :number لـ :name",
    'umrah_request_assigned' => 'تم تعيين طلب العمرة',
    'you_have_new_umrah_request' => 'لديك طلب عمرة جديد رقم :number',
    'umrah_request_accepted' => 'تم قبول طلب العمرة',
    'your_umrah_has_been_completed' => 'تم تنفيذ عمرةك رقم :number',
    'umrah_request_completed' => 'تم أكتمال طلب العمرة',
    'your_umrah_has_been_completed' =>'تم اكتمال عمرتك رقم :number الآن من قبل :name',
    'umrah_request_started' => 'تم بدء عمرتك ',
    'your_umrah_started_now' => 'تم بدء عمرتك رقم :number الآن من قبل :name',
    'new_mansk_checked'=>"تم تحقيق منسك جديد",
    "mansk_checked_for_request" => "تم تحقيق منسك :name جديد لطلب عمرة رقم :number",
    'order_status_updated' => 'تم تحديث حالة الطلب',
    'order_number_status_updated_to'=>'تم تحديث حالة الطلب رقم :number لـ :status',
];
