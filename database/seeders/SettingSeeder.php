<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    public function run()
    {
        $keys = [
            'app_logo' => [
                'page' => 'general',
                'type' => 'image',

            ],
            'dashboard_logo' =>
            [
                'page' => 'general',
                'type' => 'image',
            ],

            'app_name' => [
                'page' => 'general',
                'type' => 'text',
                'translated' => 1,
            ],
            'email' => [
                'page' => 'general',
                'type' => 'email',
            ],
            'phone' => [
                'page' => 'general',
                'type' => 'text',
            ],
            'instagram_link' => [

                'page' => 'social_links',
                'type' => 'text',
            ],
            'facebook_link' => [

                'page' => 'social_links',
                'type' => 'text',
            ],
            'twitter_link' => [

                'page' => 'social_links',
                'type' => 'text',
            ],
            'snapchat_link' => [

                'page' => 'social_links',
                'type' => 'text',
            ],
            'google_play_link' => [
                'page' => 'app_links',
                'type' => 'text',
            ],
            'app_store_link' => [
                'page' => 'app_links',
                'type' => 'text',
            ],

            'max_time_to_start_umrah' => [
                'page' => 'umrah',
                'type' => 'number',
            ],
            'max_time_allowed_to_change_status' => [
                'page' => 'umrah',
                'type' => 'number',
            ]
            ,
            'intro_title_1' => [
                'page' => 'introduction',
                'type' => 'text',
                'translated' => 1,
            ],
            'intro_title_2' => [

                'page' => 'introduction',
                'type' => 'text',
                'translated' => 1,
            ],
            'intro_title_3' => [

                'page' => 'introduction',
                'type' => 'text',
                'translated' => 1,
            ],
            'intro_title_4' => [

                'page' => 'introduction',
                'type' => 'text',
                'translated' => 1,
            ],

            'intro_paragraph_1' => [

                'page' => 'introduction',
                'type' => 'textarea',
                'translated' => 1,
            ],
            'intro_paragraph_2' => [

                'page' => 'introduction',
                'type' => 'textarea',
                'translated' => 1,

            ],
            'intro_paragraph_3' => [

                'page' => 'introduction',
                'type' => 'textarea',
                'translated' => 1,

            ],
            'intro_paragraph_4' => [

                'page' => 'introduction',
                'type' => 'textarea',
                'translated' => 1,
            ],

        ];

        foreach ($keys as $key => $value) {
            $old_setting = Setting::where('key', $key)->first();
            if ($old_setting) {
                $replicate = $old_setting->replicate();
                $replicate->save();
                $old_setting->delete();
            } else {
                $setting = Setting::create([
                    'key' => $key,
                    'type' => $value['type'],
                    'page' => $value['page'],
                    'translated' => isset($value['translated']) ? $value['translated'] : 0,

                ]);

            }
        }

    }
}
