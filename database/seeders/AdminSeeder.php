<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Admin::firstOrCreate([
            "id" => 1,
            ],
            [
            'name' => 'Super Admin',
            "email" => "admin@app.com",
            "phone" => "01000000000",
            "password" => '12345678',
        ]);
    }
}
