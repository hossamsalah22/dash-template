<?php

namespace Database\Factories;

use App\Models\Slider;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Slider>
 */
class SliderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
       $faker=[
        'is_active' => $this->faker->boolean,
       ];
        foreach (config('translatable.locales') as $locale) {
            $faker['title:' . $locale] = $this->faker->name;
        }
        return $faker;
    }

    public function configure()
    {
        return $this->afterCreating(function (Slider $slider) {

            $imagePath = public_path('seeder/images/slider.jpg');
            $collectionName = 'sliders';
            $slider->copyMedia($imagePath)
                ->toMediaCollection($collectionName);
        });
    }
}
