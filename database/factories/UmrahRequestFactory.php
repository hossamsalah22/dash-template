<?php

namespace Database\Factories;

use App\Enums\UmrahRequestEnum;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Collection;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\UmrahRequest>
 */
class UmrahRequestFactory extends Factory
{
    private Collection $users;
    private Collection $agents;
    private Collection $coupons;
    private Collection $packages;
    private Collection $user_status;

    public function __construct($count = null, ?Collection $states = null, ?Collection $has = null, ?Collection $for = null, ?Collection $afterMaking = null, ?Collection $afterCreating = null, $connection = null, ?Collection $recycle = null)
    {
        parent::__construct($count, $states, $has, $for, $afterMaking, $afterCreating, $connection, $recycle);
        $this->users = \App\Models\User::pluck('id');
        $this->agents = \App\Models\Agent::pluck('id');
        $this->coupons = \App\Models\Coupon::where('type', 'ummra')->pluck('id')->push(null)->push(null);
        $this->packages = \App\Models\UmrahPackage::pluck('id');
        $this->user_status = \App\Models\Option::where('key', 'user_status')->pluck('id');
    }

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        $data = [
            "request_number" => $this->faker->uuid,
            'user_id' => $this->users->random(),
            'agent_id' => $this->agents->random(),
            'coupon_id' => $this->coupons->random(),
            'umrah_package_id' => $this->packages->random(),
            'user_status_id' => $this->user_status->random(),
            'status' => $this->faker->randomElement(UmrahRequestEnum::getDBStatuses()),
            'total_price' => $this->faker->randomFloat(2, 1000, 10000),
            'discount' => $this->faker->randomFloat(2, 1000, 10000),
            'transaction_id' => $this->faker->uuid,
            'gender'=>$this->faker->randomElement(['male','female']),
            "name" => $this->faker->name,
            "start_after" => $this->faker->dateTimeBetween('-1 year', '+1 year'),
            "notes"=> ''
        ];
        $data['subtotal'] = $data['total_price'] + $data['discount'];

        return $data;
    }
}
