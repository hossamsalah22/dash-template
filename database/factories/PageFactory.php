<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Page>
 */
class PageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $faker=[
            'is_active'=>$this->faker->boolean,
           ];

            foreach (config('translatable.locales') as $locale) {
                $faker['title:' . $locale] = $this->faker->sentence;
                $faker['content:' . $locale] = $this->faker->paragraph;

            }
            return $faker;
    }
}
