<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Mansk>
 */
class ManskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
       $faker=[
        'order'=> $this->faker->unique()->numberBetween(1,100),
       ];

        foreach (config('translatable.locales') as $locale) {
            $faker['title:' . $locale] = $this->faker->title;
            $faker['description:' . $locale] = $this->faker->sentence;

        }
        return $faker;
    }
}
