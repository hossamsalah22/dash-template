<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Agent>
 */
class AgentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            "phone" => fake()->unique()->phoneNumber(),
            'email_verified_at' => fake()->randomElement([now(),null]),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'locale'=>fake()->randomElement(config('translatable.locales')),
            'country' => fake()->countryCode(),
            "gender" =>fake()->randomElement(['male','female']),
            "education" =>fake()->randomElement(['bachelor','master','phd']),
            "birth_date" =>fake()->date(),
            "national_id" =>fake()->unique()->randomNumber(),
            "bank_number" =>fake()->unique()->randomNumber(),
            "manask_knowledge" =>true,
            "ummra_performed" =>true,
            "is_residental" =>true,
            "nafath_verified" =>fake()->randomElement([true,false]),


        ];
    }
}
