<?php

namespace Database\Factories;

use App\Models\Option;
use App\Models\Shop;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Shop>
 */
class ShopFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        foreach (config('translatable.locales') as $locale) {
            $faker['name:' . $locale] = $this->faker->name;
            $faker['description:' . $locale] = $this->faker->sentence;

        }


        return $faker;
    }
    public function configure()
    {
        return $this->afterCreating(function (Shop $shop) {
            $locations = Option::where('is_active', 1)->where('key' , 'shop_location')->inRandomOrder()->limit(rand(1, 2))->pluck('id');
            $shop->locations()->attach($locations);
            $imagePath = public_path('seeder/images/shop.png');
            $collectionName = 'shops';
            $shop->copyMedia($imagePath)
                ->toMediaCollection($collectionName);
        });
    }

}
