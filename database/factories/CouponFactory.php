<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Coupon>
 */
class CouponFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'coupon' => $this->faker->unique()->randomNumber(6),
            "type" => $this->faker->randomElement(['shop', 'ummra']),
            "discount_type" => $this->faker->randomElement(['fixed', 'percent']), // "fixed" or "percentage
            'value' => $this->faker->numberBetween(1, 100),
            'max_usage' => $this->faker->numberBetween(1, 100),
            'max_usage_per_user' => $this->faker->numberBetween(1, 100),
            'start_at' => $this->faker->dateTimeBetween('-1 year', 'now'),
            'expires_at' => $this->faker->dateTimeBetween('now', '+1 year'),
            'is_active' => $this->faker->boolean,
            'created_at' => $this->faker->dateTimeBetween('-1 year', 'now'),
        ];
    }
}
