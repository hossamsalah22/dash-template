<?php

namespace Database\Factories;

use App\Models\Option;
use App\Models\Shop;
use App\Models\ShopItem;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\ShopItem>
 */
class ShopItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $faker= [

            'price' => $this->faker->randomFloat(2, 0, 1000),

            'shop_id' => Shop::all()->random()->id,

        ];


        foreach (config('translatable.locales') as $locale) {
            $faker['name:' . $locale] = $this->faker->name;
            $faker['description:' . $locale] = $this->faker->sentence;

        }


        return $faker;
    }
    public function configure()
    {
        return $this->afterCreating(function (ShopItem $shopItem) {

            $imagePath = public_path('seeder/images/shopItem.jpg');
            $collectionName = 'shopItems';
            $shopItem->copyMedia($imagePath)
                ->toMediaCollection($collectionName);
        });
    }
}
