<?php

namespace Database\Factories;

use App\Models\Fatwa;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Fatwa>
 */
class FatwaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $faker=[
            'author' => $this->faker->name,
            'is_active' => $this->faker->boolean,
            'type' => $this->faker->randomElement(['fatwa', 'ahadith']),
        ];
        foreach (config('translatable.locales') as $locale) {
            $faker['title:' . $locale] = $this->faker->title;
            $faker['content:' . $locale] = $this->faker->text;

        }


        return $faker;
    }
    public function configure()
    {
        return $this->afterCreating(function (Fatwa $fatwa) {
            $imagePath = public_path('seeder/images/Fatwa.jpg');
            $collectionName = 'fatwas';
            $fatwa->copyMedia($imagePath)
                ->toMediaCollection($collectionName);
        });
    }
}
