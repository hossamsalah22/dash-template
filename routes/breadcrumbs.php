<?php

use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;
use Illuminate\Support\Facades\Route;

Breadcrumbs::for('home', function (BreadcrumbTrail $trail) {
    $trail->push(__('translate.home'), route('dashboard.home'));
});
Breadcrumbs::for('resource', function (BreadcrumbTrail $trail, $parameter = null) {

    $currentRoute = Route::current();
    $routeName = $currentRoute->getName();

    $trail->parent('home');

    $parts = explode('.', $routeName);

    $entity = $parts[1] ?? null;
    $operation = $parts[2] ?? null;

    if ($entity ) {
        $entityRoute = 'dashboard.' . $entity . '.index';
        $entityName = __('translate.' . $entity);
        if(Route::has($entityRoute)) {
            $trail->push($entityName, route($entityRoute));
        }
    }

    if ($operation) {
        $operationName = __('translate.' . $operation);
        $trail->push($operationName);
    }

    if ($parameter) {
        $trail->push($parameter);
    }

});

Breadcrumbs::for("options.index", function (BreadcrumbTrail $trail , $key) {

    $trail->parent('home');
    $trail->push(__('translate.' . $key), route('dashboard.key.options.index' , $key));
});

// options create
Breadcrumbs::for("options.create", function (BreadcrumbTrail $trail , $key) {

    $trail->parent('options.index', $key);
    $trail->push(__('translate.create') , route('dashboard.key.options.create' , $key));
});

// options edit

Breadcrumbs::for("options.edit", function (BreadcrumbTrail $trail , $key , $id) {

    $trail->parent('options.index', $key);
    $trail->push(__('translate.edit') , route('dashboard.key.options.edit' , [$key , $id]));
});

// options show


Breadcrumbs::for("options.show", function (BreadcrumbTrail $trail , $key , $id) {

    $trail->parent('options.index', $key);
    $trail->push(__('translate.show') , route('dashboard.key.options.show' , [$key , $id]));
});
Breadcrumbs::for('shops.index', function (BreadcrumbTrail $trail , $shop) {
    $trail->parent('home');
    $trail->push(__('translate.shops'), route('dashboard.shops.index'));
    $trail->push($shop->name , route('dashboard.shops.index'));
});

Breadcrumbs::for('shops.items.index' , function ( BreadcrumbTrail $trail , $shop) {

    $trail->parent('shops.index' , $shop);
    $trail->push(__('translate.items') , route('dashboard.shops.items.index' , $shop->id));

});
Breadcrumbs::for('shops.items.create' , function ( BreadcrumbTrail $trail , $shop) {
    $trail->parent('shops.items.index' , $shop);

    $trail->push(__('translate.create') , route('dashboard.shops.items.create' , $shop->id));

});
Breadcrumbs::for('shops.items.edit' , function ( BreadcrumbTrail $trail , $shop , $item) {
    $trail->parent('shops.items.index' , $shop);

    $trail->push(__('translate.edit') , route('dashboard.shops.items.edit' , [$shop->id , $item]));

});





