<?php

use App\Http\Controllers\Dashboard\AdminController;
use App\Http\Controllers\Dashboard\AgentController;
use App\Http\Controllers\Dashboard\ContactController;
use App\Http\Controllers\Dashboard\CouponController;
use App\Http\Controllers\Dashboard\FatwaController;
use App\Http\Controllers\Dashboard\HomeController;
use App\Http\Controllers\Dashboard\ManskController;
use App\Http\Controllers\Dashboard\NotificationController;
use App\Http\Controllers\Dashboard\OptionController;
use App\Http\Controllers\Dashboard\OrderController;
use App\Http\Controllers\Dashboard\PageController;
use App\Http\Controllers\Dashboard\SettingController;
use App\Http\Controllers\Dashboard\ShopController;
use App\Http\Controllers\Dashboard\ShopItemController;
use App\Http\Controllers\Dashboard\SliderController;
use App\Http\Controllers\Dashboard\UmrahPackageController;
use App\Http\Controllers\Dashboard\UmrahRequestsController;
use App\Http\Controllers\Dashboard\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
//  */
// Route::middleware('guest:admin')->group(function () {

//    Route::get('/login', [AuthController::class, 'login'])->name('login');
//    Route::post('/login', [AuthController::class, 'submitLogin'])->name('submit-login');
// });

Auth::routes([
    'register' => false,
    'reset' => true,
    'verify' => false,
]);

Route::middleware(['auth:admin', 'is_active'])->group(function () {

    Route::get('/', HomeController::class)->name('home');

    //Settings
    Route::get('/settings/create', [SettingController::class, 'create'])->name('settings.create');
    Route::post('/settings/store', [SettingController::class, 'store'])->name('settings.store');
    /* CONTACT US MESSAGE ACTIONS */
    Route::post('/messages/{contact}/read', [\App\Http\Controllers\Dashboard\ContactController::class,'read'])->name('contacts.read');
    Route::post('/messages/{contact}/replied', [\App\Http\Controllers\Dashboard\ContactController::class,'replied'])->name('contacts.replied');
    Route::get('/notifications', [NotificationController::class,'index'])->name('notifications.index');
    Route::get('/notifications/{notification}', [NotificationController::class,'show'])->name('notifications.show');







    //Toggle
    Route::post('/admins/{admin}/toggle', [AdminController::class, 'toggle'])->name('admins.toggle');

    Route::post('/pages/{page}/toggle', [PageController::class, 'toggle'])->name('pages.toggle');

    Route::post('/options/{option}/toggle', [OptionController::class, 'toggle'])->name('key.options.toggle');

    Route::post('/sliders/{slider}/toggle', [SliderController::class, 'toggle'])->name('sliders.toggle');
    Route::delete('options/{option}/destroy', [OptionController::class, 'destroy'])->name('key.options.destroy');


    //Resources
    Route::resource('contacts', ContactController::class)->only(['show','index','destroy']);
    Route::resource('admins', AdminController::class)->except(['show']);
    Route::resource('pages', PageController::class)->except(['show']);

    Route::resource('key.options', OptionController::class)->except(['show', 'destroy']);
    Route::resource('sliders', SliderController::class)->except(['show']);
});
