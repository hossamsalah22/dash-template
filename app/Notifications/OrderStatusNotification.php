<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use \NotificationChannels\Fcm\Resources\Notification as FcmNotification;

class OrderStatusNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(private Order $order)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return [FcmChannel::class];
    }
    public function toFcm(object $notifiable)
    {
        $data=$this->justifyData($notifiable);
        \Log::info($data);
       return FcmMessage::create()
       ->notification(FcmNotification::create(...$data));


    }

    protected function justifyData($notifiable)
    {
        return [
            'title' => __('notifications.order_status_updated',[] , $notifiable->locale),
            'body' => __('notifications.order_number_status_updated_to' ,['status'=>__('translate.'.$this->order->status) , 'number'=>$this->order->order_number] , $notifiable->locale),

        ];
    }
    /**
     * Get the mail representation of the notification.
     */

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
