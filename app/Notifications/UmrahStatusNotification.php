<?php

namespace App\Notifications;

use App\Services\FcmNotificationService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;
use \NotificationChannels\Fcm\Resources\Notification as FcmNotification;


class UmrahStatusNotification extends Notification implements ShouldQueue
{
    use Queueable;


    /**
     * Create a new notification instance.
     */
    public function __construct(private $notification)
    {

    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */


     public function via(object $notifiable): array
     {
         return [FcmChannel::class];
     }
     public function toFcm(object $notifiable)
     {
        \Log::info($this->notification);
        return FcmMessage::create()
        ->notification(FcmNotification::create(...$this->notification));


     }




    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */

}
