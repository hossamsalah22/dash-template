<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class VerifyCodeEmail extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(public string $subject,public int $expires = 60)
    {
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        $notifiable->verification_code = random_int(1000, 9999);
        $notifiable->verification_code_expired_at = now()->addMinutes($this->expires??config('auth.verification.expire', 60));
        $notifiable->save();

        app()->setLocale($notifiable->locale);

        return (new MailMessage)
                    ->subject(__($this->subject,locale:$notifiable->locale))
                    ->line(__('Please use the following code to verify your email address.'))
                    ->line(__("Your verification code is",["code"=>$notifiable->verification_code]))
                    ->line(__("The verification code will expire after",['time'=>$notifiable->verification_code_expired_at->diffForHumans()]))
                    ->line(__('If you did not create an account, no further action is required.'))
                    ->line(__('Thank you for using our application!'));

    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
