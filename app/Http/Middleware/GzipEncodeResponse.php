<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class GzipEncodeResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if(!env('GZIP_ENABLED', false))
            return $next($request);


        $response = $next($request);

        $response->setContent(gzencode($response->getContent(),6));
        $response->headers->set('Content-Encoding', 'gzip');
        $response->headers->set('Content-Length', strlen($response->getContent()));

        return $response;
    }
}
