<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        if ($request->hasHeader('Accept-Language')) {
            $lang = $request->header('Accept-Language');
            if (in_array($lang, config('translatable.locales'))) {
                app()->setLocale($lang);
            }
        }
        else if(auth('agent-api')->check()){
            app()->setLocale(auth('agent-api')->user()->locale);
        }
        elseif(auth('user-api')->check()){
            app()->setLocale(auth('user-api')->user()->locale);
        }


        return $next($request);
    }
}
