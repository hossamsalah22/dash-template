<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class Active
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (auth()->check() && !auth()->user()->is_active) {
            if($request->ajax() || $request->wantsJson() || $request->expectsJson()){
//                Auth::user()->tokens()->delete();
                return response()->json([
                    'message' => __('translate.account_disabled'),
                ], 401);
            }
            auth()->logout();
            return redirect()->back()->with('error', __('translate.account_disabled'));
        }
        return $next($request);
    }
}
