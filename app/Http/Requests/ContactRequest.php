<?php

namespace App\Http\Requests;


class ContactRequest extends AuthorizedRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255|min:3',
            'email' => 'required|email|max:255',
            'phone' => 'required|digits_between:10,15',
            'subject_id' => 'required|exists:options,id,key,subject',
            'message' => 'required|string|max:65535|min:10',
        ];
    }
}
