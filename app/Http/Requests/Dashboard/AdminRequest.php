<?php

namespace App\Http\Requests\Dashboard;

use App\Http\Requests\AuthorizedRequest;

class AdminRequest extends AuthorizedRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $id = $this->route('admin')?->id;
        return [
            'name' => 'required|min:3',
            'email' => 'email|unique:admins,email,' . $id,
            'phone' => 'required|digits:11|unique:admins,phone,' . $id,
            'password' => [$id?'nullable':"required","min:8", "confirmed"],
            'password_confirmation' => ["required_with:password"],
        ];
    }
}
