<?php

namespace App\Http\Requests\Dashboard;

use App\Http\Requests\AuthorizedRequest;
use Astrotomic\Translatable\Validation\RuleFactory;

class PageRequest extends AuthorizedRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return RuleFactory::make([
            '%title%' => 'required|string|min:3|max:255',
            '%content%' => ['required', 'string',"min:10"],
        ]);
    }
}
