<?php

namespace App\Http\Requests\Dashboard;

use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $id = $this->route('slider')?->id;

        $rules= RuleFactory::make([
            '%title%' => 'nullable|string|min:3|max:255',
        ]);

        $rules['image'] = [$id?'nullable':"required" , 'image' , 'max:2048' , 'mimes:jpeg,jpg,png,gif'];

        return $rules;
    }
}
