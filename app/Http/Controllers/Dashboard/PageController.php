<?php

namespace App\Http\Controllers\Dashboard;

use App\Datatables\PageDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\PageRequest;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        if(request()->ajax())
        {
            return (new PageDatatable(new Page))->getData();
        }
        return view('dashboard.pages.index');
    }

    public function create()
    {
        return view('dashboard.pages.create');
    }

    public function store(PageRequest $request)
    {
        Page::create($request->validated());

        return redirect()->route('dashboard.pages.index')->with('success', __('translate.added_successfully'));
    }

    public function edit(Page $page)
    {
        return view('dashboard.pages.edit', compact('page'));
    }

    public function update(PageRequest $request, Page $page)
    {
        $page->update($request->validated());

        return redirect()->route('dashboard.pages.index')->with('success', __('translate.updated_successfully'));
    }

    public function destroy(Page $page)
    {
        $page->delete();

        return response()->json([
            'message' => __('translate.deleted_successfully')
        ]);
    }

    public function order(Page $page)
    {
        //
    }

    public function toggle(Page $page)
    {
        $page->update(['is_active' => !$page->is_active]);

        return response()->json([
            'message' => __('translate.updated_successfully')
        ]);
    }
}
