<?php

namespace App\Http\Controllers\Dashboard;

use App\Datatables\SliderDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\SliderRequest;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        if(request()->ajax())
        {
            return (new SliderDatatable(new Slider()))->getData();
        }
        return view('dashboard.sliders.index');
    }

    public function create()
    {
        return view('dashboard.sliders.create');
    }

    public function store(SliderRequest $request)
    {
        Slider::create($request->validated());

        return redirect()->route('dashboard.sliders.index')->with('success', __('translate.added_successfully'));
    }

    public function edit(Slider $slider)
    {
        return view('dashboard.sliders.edit', compact('slider'));
    }

    public function update(SliderRequest $request, Slider $slider)
    {
        $slider->update($request->validated());
        if($request->has('image')){

        $slider->storeMediaFromRequest();
        }
        return redirect()->route('dashboard.sliders.index')->with('success', __('translate.updated_successfully'));
    }

    public function destroy(Slider $slider)
    {
        $slider->delete();

        return response()->json([
            'message' => __('translate.deleted_successfully')
        ]);
    }
    public function toggle(Slider $slider, Request $request)
    {
        $slider->update([
            $request->property => $request->value,
        ]);

        return response()->json([
            'message' => __('translate.updated_successfully')
        ]);
    }


}
