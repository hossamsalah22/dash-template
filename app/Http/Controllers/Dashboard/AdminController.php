<?php

namespace App\Http\Controllers\Dashboard;

use App\Datatables\AdminDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\AdminRequest;
use App\Models\Admin;

class AdminController extends Controller
{
    public function index()
    {
        if(request()->ajax())
        {
            return AdminDatatable::getData();
        }
        return view('dashboard.admins.index');
    }
    public function store(AdminRequest $request)
    {
        Admin::create($request->validated());

        return redirect()->route('dashboard.admins.index')->with('success', __('translate.created_successfully'));
    }
    public function create()
    {
        return view('dashboard.admins.create');
    }


    public function edit(Admin $admin)
    {
        return view('dashboard.admins.edit', compact('admin'));
    }
     public function update(AdminRequest $request, Admin $admin)
    {
        $data = $request->validated();
        if(!$data['password']){
            unset($data['password']);
        }
        $admin->update($data);

        return redirect()->route('dashboard.admins.index')->with('success', __('translate.updated_successfully'));
    }

    public function destroy(Admin $admin)
    {
        if($admin->email == "admin@app.com"){
            return response()->json([
                'message' => __('translate.cant_delete_admin')
            ], 403);
        }
        $admin->delete();
        return response()->json([
            'message' => __('translate.deleted_successfully')
        ]);
    }

    public function toggle(Admin $admin)
    {

        if($admin->email == "admin@app.com"){
            return response()->json([
                'message' => __('translate.cant_toggle_admin')
            ], 403);
        }
        if($admin->id == auth()->id()){
            return response()->json([
                'message' => __('translate.cant_toggle_yourself')
            ], 403);
        }

        $admin->is_active = !$admin->is_active;
        $admin->save();

        return response()->json([
            'message' => __('translate.updated_successfully')
        ]);
    }
}
