<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function create()
    {if (request()->has('page')) {
        $page = request('page');
    } else {
        return abort(404);
    }
        $options = Setting::with(['translations' , 'media'])->where('page', $page)->get();
        return view('dashboard.settings.create', ['options' => $options, 'page' => $page]);
    }
    public function store(Request $request)
    {
        $settings = Setting::with(['translations' , 'media'])->get();
        abort_if($settings->isEmpty(), 404);

        foreach ($request->all() as $key => $item) {
            $this->processSetting($key, $item, $settings);
        }
        return redirect()->back()->with('success', __('translate.updated_successfully'));
    }

    private function processSetting($key, $item, $settings)
    {
        $setting = $settings->where('key', $key)->first();
        if (!$setting) {
            return;
        }
        if (in_array($setting->type, ['image', 'video']) && $this->hasFile($key, 'image')) {
            $this->storeMedia($setting, $key);
        } else {
            $setting->update($item);

        }
    }
    private function hasFile($key, $type)
    {

        return request()->hasFile("{$key}.{$type}");
    }

    private function storeMedia($setting, $key)
    {
        $setting->clearMediaCollection('settings');

        $fileKey = "{$key}.image";
        $filename = $setting->value . '.' . request()->file($fileKey)->getClientOriginalExtension();

        $setting->addMediaFromRequest($fileKey)
            ->usingFileName($filename)
            ->toMediaCollection('settings');
    }

}
