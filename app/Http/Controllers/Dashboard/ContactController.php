<?php

namespace App\Http\Controllers\Dashboard;

use App\Datatables\ContactDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\Http\Resources\OptionResource;
use App\Models\Contact;
use Illuminate\Support\Str;

class ContactController extends Controller
{

    public function index()
    {
        if(request()->ajax()){
            return (new ContactDatatable(new Contact))->getData();
        }
        return view('dashboard.contacts.index');
    }
    public function show(Contact $contact)
    {
        return view('dashboard.contacts.show',compact('contact'));
    }

    public function destroy(Contact $contact)
    {
        $contact->delete();
        return response()->json([
            'message' => __('translate.deleted_successfully'),
        ]);
    }

    public function read(Contact $contact)
    {
        $contact->update([
            'is_read' => !$contact->is_read,
        ]);
        return redirect()->back()->with('success', __('translate.updated_successfully'));
    }
    public function replied(Contact $contact)
    {
        $contact->update([
            'is_replied' => !$contact->is_replied,
        ]);
        return redirect()->back()->with('success', __('translate.updated_successfully'));
    }






}
