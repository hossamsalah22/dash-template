<?php

namespace App\Http\Controllers\Dashboard;

use App\Datatables\AdminDatatable;
use App\Datatables\NotificationDatatable;
use App\Http\Controllers\Controller;
use App\Models\AdminNotification;

class NotificationController extends Controller
{
    public function index()
    {
        if(request()->ajax()){
            return (new NotificationDatatable(new AdminNotification))->getData();
        }
        return view('dashboard.notifications.index');
    }

    public function show(AdminNotification $notification)
    {
        if(!$notification->read_at){
            $notification->update(['read_at' => now(),'read_by' => auth()->id()]);
        }
        return redirect($notification->link);
    }

    public function markAsRead(AdminNotification $notification)
    {
        $notification->update(['read_at' => now()]);

        return response()->json([
            'status' => true,
//            'message' => __('notifications.marked_as_read'),
        ]);
    }
}
