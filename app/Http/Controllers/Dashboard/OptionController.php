<?php

namespace App\Http\Controllers\Dashboard;

use App\Datatables\OptionDatatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\OptionRequest;
use App\Http\Requests\Dashboard\ShopRequest;
use App\Models\Option;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    private $datatable;

    public function __construct(private $model = null, private $viewsDomain = null , OptionDatatable  $datatable = null)
    {
        $this->model = new Option();
        $this->datatable = $datatable;
        $this->viewsDomain = 'dashboard.options.';

    }

    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    /**
     * Display a listing of the resource.
     */
    public function index($key)
    {
        if(request()->ajax()){

        return $this->datatable->getData($key);

        }

        return $this->view('index' , compact('key'));

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create($key)
    {

        return $this->view('create' , compact('key'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store($key,OptionRequest $request)
    {

        $data=array_merge($request->validated() , ['key' => $key] ) ;
        $this->model->create($data);

        return redirect(route('dashboard.key.options.index' , compact('key')))->with('success', __('translate.created_successfully'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($key,Option $option)
    {

        return $this->view('edit', compact('key' , 'option'));

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(OptionRequest $request,$key , Option $option)
    {
        $option->update($request->validated());
        return redirect(route('dashboard.key.options.index' , compact('key')))->with('success', __('translate.updated_successfully'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Option $option)
    {
        $option->delete();
        return response()->json([
            'message' => __('translate.deleted_successfully')
        ]);
    }

    public function toggle(Option $option, Request $request)
    {
        $option->update([
            $request->property => $request->value,
        ]);
        return response()->json([
            'message' => __('translate.updated_successfully')
        ]);


    }
}
