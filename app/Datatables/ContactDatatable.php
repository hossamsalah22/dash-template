<?php

namespace App\Datatables;

use App\Models\Contact;
use Yajra\DataTables\Facades\DataTables;

class ContactDatatable
{

    public function __construct(private readonly Contact $model)
    {
    }

    public function getData()
    {
        $data = $this->model->latest()->select('*');

        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('subject', function ( $model) {
                return $model->subject->value;
            })
            ->editColumn('is_read', function ( $model) {
                return $model->is_read ? __('translate.yes') : __('translate.no');
            })
            ->editColumn('is_replied', function ( $model) {
                return $model->is_replied ? __('translate.yes') : __('translate.no');
            })

            ->editColumn('created_at', function ( $model) {
                return optional($model->created_at)->format('Y-m-d');
            })
            ->addColumn('actions', function ($row) {
                return view('components.datatable.actions', [
                    'id' => $row->id,
                    'route' => 'contacts',
                    'delete'=>1,
                    'edit'=>0,
                    'show'=>1,
                ])->render();
            })
            ->rawColumns(['is_read','is_replied','subject','actions'])
            ->toJson();
    }
}
