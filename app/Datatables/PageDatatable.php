<?php

namespace App\Datatables;

use App\Models\Page;
use Yajra\DataTables\Facades\DataTables;

class PageDatatable
{

    public function __construct(private readonly Page $model)
    {
    }

    public function getData()
    {
        $data = $this->model->with('translations')->latest()->select('*');

        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('created_at', function ( $model) {
                return optional($model->created_at)->format('Y-m-d');
            })
            ->editColumn('is_active', function ( $model) {
                return view('components.datatable.switch',[
                    'id'=>$model->id,
                    'active'=>$model->is_active,
                ]);
            })
            ->addColumn('actions', function ($row) {
                return view('components.datatable.actions', [
                    'id' => $row->id,
                    'route' => 'pages',
                    'delete'=>1,
                    'edit'=>1,
                ])->render();
            })
            ->rawColumns(['actions'])
            ->toJson();
    }
}
