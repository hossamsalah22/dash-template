<?php

namespace App\Datatables;

use App\Models\Admin;
use Yajra\DataTables\Facades\DataTables;
use function Termwind\render;

class AdminDatatable
{
    public static function getData()
    {
        $data = Admin::latest()->select('*');

        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('is_active', function (Admin $model) {
                return view('components.datatable.switch',[
                    'id'=>$model->id,
                    'active'=>(int)$model->is_active,
                ])->render();
            })
            ->editColumn('created_at', function (Admin $model) {
                return optional($model->created_at)->format('Y-m-d');
            })
            ->addColumn('actions', function ($row) {
                return view('components.datatable.actions', [
                    'id' => $row->id,
                    'route' => 'admins',
                    'delete'=>1,
                    'edit'=>1,
                ])->render();
            })
            ->rawColumns(['actions', 'is_active'])
            ->toJson();
    }
}
