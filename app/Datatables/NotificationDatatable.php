<?php

namespace App\Datatables;

use App\Models\Admin;
use App\Models\AdminNotification;
use Yajra\DataTables\Facades\DataTables;

class NotificationDatatable
{

    public function __construct(private readonly AdminNotification $model)
    {
    }

    public function getData()
    {
        $data = $this->model->latest()->with('admin');

        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('created_at', function ($model) {
                return optional($model->created_at)->format('Y-m-d');
            })
            ->editColumn('read_at', function ($model) {
                return optional($model->read_at)->diffForHumans();
            })
            ->editColumn('link', function ($model) {
                $link = route('dashboard.notifications.show', $model->id);
                return !$model->link ? "" : "<a href='{$link}'>". __('translate.visit')."</a>";
            })
            ->editColumn('read_by', function ($model) {
                return $model->admin ? $model->admin->name : "";
            })

            ->RawColumns(['link', 'read_at', 'read_by'])
            ->toJson();
    }
}
