<?php

namespace App\Datatables;

use App\Models\Shop;
use App\Models\Slider;
use Yajra\DataTables\Facades\DataTables;

class SliderDatatable
{
    public Slider $model;

    public function __construct(Slider $model)
    {
        $this->model = $model;
    }

    public function getData()
    {
        $data = $this->model->latest()->select('*');

        return DataTables::of($data)
            ->addIndexColumn()
            ->editcolumn('image', function (Slider $model) {
                return '<img src="' . asset($model->thumbnail) . '" width="50px" height="50px">';
            })
            ->editColumn('created_at', function (Slider $model) {
                return optional($model->created_at)->format('Y-m-d');
            })
            ->editColumn('is_active', function (Slider $model) {
                return view('components.datatable.switch', [
                    'id' => $model->id,
                    'active' => $model->is_active,
                ])->render();
            })
            ->addColumn('actions', function ($row) {
                return view('components.datatable.actions', [
                    'id' => $row->id,
                    'route' => 'sliders',
                    'delete' => 1,
                    'edit' => 1,
                ])->render();
            })
            ->rawColumns(['actions', 'image', 'is_active'])
            ->toJson();
    }
}
