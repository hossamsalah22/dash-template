<?php

namespace App\Datatables;

use App\Models\Option;
use Yajra\DataTables\Facades\DataTables;

class OptionDatatable
{


    public function __construct(private readonly Option $model)
    {
    }

    public function getData($key)
    {
        $data = $this->model->with('translation')->where('key', $key)->select('*');

        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('created_at', function (Option $model) {
                return optional($model->created_at)->format('Y-m-d');
            })
            ->editColumn('is_active', function (Option $model) {
                return view('components.datatable.switch',[
                    'id'=>$model->id,
                    'active'=>$model->is_active,
                ])->render();
            })

            ->addColumn('actions', function ($row) use ($key) {
                return view('components.datatable.actions', [
                    'id' => $row->id,
                    'route' => 'key.options',
                    'param'=>$key,
                    'delete'=>1,
                    'edit'=>1,
                ])->render();
            })
            ->rawColumns(['actions' , 'is_active'])
            ->toJson();
    }
}
