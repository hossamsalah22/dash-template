<?php

namespace App\Console\Commands;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;

use function Laravel\Prompts\text;

class CreateModelWithTranslationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    #php artisan make:transModel Item name,desc
    protected $signature = 'make:transModel
        {model? : The name of the model}
        {attributes? : [Optional] The attributes to be translated (comma separated)}
        ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a model with translation setup';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {

//        dd($this->argument('model'),$this->argument('attributes'));

        $modelName = $this->argument('model')
            ??
            text(
                'Enter model name',
                placeholder:'Item',
                required: true,
                hint: 'e.g. Item'
            );
        $this->input->setArgument('model', $modelName);

        $attributes = $this->argument('attributes')
            ??
            text(
                'Enter attributes to be translated (comma separated)',
                placeholder: 'e.g. name,description',
                hint: 'e.g. name,description',

            );
        $this->input->setArgument('attributes', $attributes);

        $translationModelName = $modelName . 'Translation';
        $tableName = Str::snake(Str::plural($modelName));
        $migrationTableName = Str::snake(Str::singular($modelName));
        $translationTableName = $migrationTableName . '_translations';

        // Create Migration
        Artisan::call('make:migration', [
            'name' => "create_{$tableName}_table"
        ]);
        $this->info("{$tableName} migration created successfully.");
        sleep(1);

        // Create Translation Migration
        Artisan::call('make:migration', [
            'name' => "create_{$translationTableName}_table"
        ]);
        $this->info("{$translationTableName} migration created successfully.");

        // Create Model
        Artisan::call('make:model', [
            'name' => $modelName
        ]);
        $this->info("{$modelName} model created successfully.");

        // Create Translation Model
        Artisan::call('make:model', [
            'name' => $translationModelName
        ]);
        $this->info("{$translationModelName} model created successfully.");

        // Modify Main Model
        $this->modifyMainModel($modelName);
        // Modify Main Model
        $this->modifyTranslationModel($translationModelName);


        // Modify Translation Migration
        $this->modifyTranslationMigration($translationTableName,$modelName);

        $this->info('Translation setup created successfully.');
    }

    protected function modifyTranslationMigration($translationTableName,$modelName): void
    {
        $migrationTablePath = glob(database_path("migrations/*_create_{$translationTableName}_table.php"));

        if(count($migrationTablePath) == 0) {
            $this->error("Migration file for {$translationTableName} does not exist.");
            return;
        }
        $migrationTablePath = $migrationTablePath[count($migrationTablePath)-1];
        $fileContents = file_get_contents($migrationTablePath);
        // Add translatable attributes
        $attributes = $this->argument('attributes');
        $attributesArray = explode(',', $attributes);
        // remove "" from array
        $attributesArray = array_filter($attributesArray, function($value) { return $value !== ''; });
        if(count($attributesArray) ) {
            $mode_name_id = Str::snake($modelName) . '_id';

            $linesToBeAdded = [
                "\$table->id();",
                "\$table->string('locale')->index();",
                "\$table->foreignId('{$mode_name_id}')->constrained()->onDelete('cascade');",
            ];

            foreach ($attributesArray as $attribute) {
                $attribute = trim($attribute);
                $linesToBeAdded[] = "\$table->string('{$attribute}');";
            }
//            add unique constraint for each attribute
            $linesToBeAdded[] = "\$table->unique(['{$mode_name_id}', 'locale']);";

            $fileContents = str_replace(
                "\$table->id();\n            \$table->timestamps();",
                implode("\n            ", $linesToBeAdded),
                $fileContents
            );
//            dump($linesToBeAdded);
//            dump($fileContents);
            file_put_contents($migrationTablePath, $fileContents);

            $this->info("{$translationTableName} migration modified successfully.");

        }
    }

    protected function modifyMainModel($modelName): void
    {
        $modelPath = app_path("Models/{$modelName}.php");

        if (!file_exists($modelPath)) {
            $this->error("Model {$modelName} does not exist.");
            return;
        }

        $fileContents = file_get_contents($modelPath);

        // Check if model already uses Translatable trait
        if (!str_contains($fileContents, 'use Translatable;')) {
            // Add Translatable trait import
            $fileContents = str_replace(
                "use Illuminate\Database\Eloquent\Model;",
                "use Illuminate\Database\Eloquent\Model;\nuse Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;\nuse Astrotomic\Translatable\Translatable;",
                $fileContents
            );

            // Add Translatable trait usage
            $fileContents = str_replace(
                "extends Model\n{\n    use HasFactory;",
                "extends Model implements TranslatableContract\n{\n    use HasFactory, Translatable;",
                $fileContents
            );
        }

        // Add translatable attributes
        $attributes = $this->argument('attributes');
        $attributesArray = explode(',', $attributes);
        // remove "" from array
        $attributesArray = array_filter($attributesArray, function($value) { return $value !== ''; });
        $attributesArray = array_map(fn($value) => trim($value), $attributesArray);


        if(count($attributesArray) ) {
            $translatableArrayString = "protected array \$translatedAttributes = ['" . implode("', '", $attributesArray) . "'];\n";
        }
        else
            $translatableArrayString = "protected array \$translatedAttributes = [];\n";

        if (!str_contains($fileContents, '$translatedAttributes')) {
            $fileContents = str_replace(
                ", Translatable;",
                ", Translatable;\n\n    " . $translatableArrayString,
                $fileContents
            );
        }

        file_put_contents($modelPath, $fileContents);

        $this->info("{$modelName} model modified successfully.");
    }

    protected function modifyTranslationModel($translationModelName):void
    {
        $modelPath = app_path("Models/{$translationModelName}.php");

        if (!file_exists($modelPath)) {
            $this->error("Model {$translationModelName} does not exist.");
            return;
        }

        $fileContents = file_get_contents($modelPath);


        $attributes = $this->argument('attributes');

        $attributesArray = explode(',', $attributes);
        // remove "" from array
        $attributesArray = array_filter($attributesArray, function($value) { return $value !== ''; });

        if(count($attributesArray) ) {
            $translatableArrayString = "protected \$fillable = ['" . implode("', '", $attributesArray) . "'];\n";
        }
        else
            $translatableArrayString = "protected array \$fillable = [];\n";

        if (!str_contains($fileContents, '$fillable')) {
            $fileContents = str_replace(
                "use HasFactory;",
                "public \$timestamps = false;\n    " . $translatableArrayString,
                $fileContents
            );

            file_put_contents($modelPath, $fileContents);
        }

        $this->info("{$translationModelName} model modified successfully.");
    }
}
