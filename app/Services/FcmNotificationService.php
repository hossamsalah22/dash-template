<?php
namespace App\Services;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Kreait\Firebase\Messaging\MulticastSendReport;
use NotificationChannels\Fcm\FcmMessage;
use \NotificationChannels\Fcm\Resources\Notification;

class FcmNotificationService
{

    const URL = 'https://fcm.googleapis.com/fcm/send';
    const TOKENS_PER_REQUEST = 500;

    private static function getClient(): \Kreait\Firebase\Contract\Messaging
    {
        return app(\Kreait\Firebase\Contract\Messaging::class);
    }
    private static function justifyData(array $data): array
    {
        if (in_array('image', array_keys($data)) && $data['image'])
        // check if image is url
        {
            if (!filter_var($data['image'], FILTER_VALIDATE_URL)) {
                $data['image'] = asset($data['image']);
            } else {
                $data['image'] = asset($data['image']);
            }
        }

        return [
            'title' => $data['title'],
            'body' => $data['body'],
            'image' => $data['image'] ?? null,
        ];
    }

    public static function sendNotification(string $token, array $notification, array $data = []): void
    {
        $notification = self::justifyData($notification);

        $message = FcmMessage::create()
            ->notification(Notification::create(...$notification));
        if (!empty($data)) {
            $message->data($data);
        }

        $report = self::getClient()->sendMulticast($message, [$token]);

        self::logFailures($report, $notification);

    }
    public static function sendBulkNotification(array | Collection | null $tokens, array $notification, array $data = []): void
    {
      

        $notification = self::justifyData($notification);

        if ($tokens instanceof Collection) {
            $tokens = $tokens->toArray();
        }

        if (empty($tokens) || !is_array($tokens)) {
            return;
        }

        $message = FcmMessage::create()
            ->notification(Notification::create(...$notification));
            if (!empty($data)) {
                $message->data($data);
            }
        $client = self::getClient();

        Collection::make($tokens)
            ->chunk(self::TOKENS_PER_REQUEST)
            ->map(fn($tokens) => $client->sendMulticast($message, $tokens->all()))
            ->map(fn(MulticastSendReport $report) => self::logFailures($report, $notification));

    }

    private static function logFailures(MulticastSendReport $report, array $data): void
    {
        $unknownTokens = $report->unknownTokens();
        $invalidTokens = $report->invalidTokens();
        Log::channel('notifications')->info('FCM Notification: ', $data);
        Log::channel('notifications')->info('FCM ' . $report->successes()->count() . ' messages were sent successfully.');
        if ($report->hasFailures()) {
            if ($unknownTokens) {
                Log::channel('notifications')->error("Unknown Tokens[" . count($unknownTokens) . "]:  ", $unknownTokens);
            }

            if ($invalidTokens) {
                Log::channel('notifications')->error("Invalid Tokens[" . count($invalidTokens) . "]:  ", $invalidTokens);
            }

        }
        Log::channel('notifications')->info('===============================================================');
    }

    public static function sendNotificationV1(string $token, array $data): bool | string | array
    {
        $fields = [
            'to' => $token,
            'notification' => [
                'title' => $data['title'],
                'body' => $data['body'],
                'image' => $data['image'] ? asset($data['image']) : null,
            ],
        ];

        $response = Http::withHeaders(self::getHeaders())
            ->post(self::URL, $fields);

        if ($response->failed()) {
            Log::error('FCM  ' . $response->body());
            return $response->json();
        }

        Log::info('FCM  ' . $response->body());
        return $response->json();
    }

    public static function sendBulkNotificationV1(array | Collection | null $tokens, array $data): bool | string | array
    {

        if ($tokens instanceof Collection) {
            $tokens = $tokens->toArray();
        }

        if (empty($tokens) || !is_array($tokens)) {
            return false;
        }

        $notification = [
            'title' => $data['title'],
            'body' => $data['body'],
            'image' => $data['image'] ? asset($data['image']) : null,
        ];
        collect($tokens)->chunk(100)->each(function ($tokens) use ($notification) {
            $fields = [
                'registration_ids' => $tokens,
                'notification' => $notification,
            ];
            $response = Http::withHeaders(self::getHeaders())->post(self::URL, $fields);

            if ($response->failed()) {
                Log::error('FCM  ' . $response->body());
                return $response->json();
            }

            Log::info('FCM  ' . $response->body());
            return $response->json();
        });

        return true;

    }

    private static function getAuthorization(): string
    {
        return 'key=' . env('FIREBASE_GOOGLE_API_KEY');
    }

    private static function getHeaders(): array
    {
        return [
            'Authorization' => self::getAuthorization(),
            'Content-Type' => 'application/json',
        ];
    }
}
