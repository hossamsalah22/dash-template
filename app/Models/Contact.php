<?php

namespace App\Models;

use App\Events\DashboardNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $with = ['subject'];

    protected $appends = ['subject_title'];

    public function getSubjectTitleAttribute()
    {
        return $this->subject->value;
    }

    public function subject()
    {
        return $this->belongsTo(Option::class, 'subject_id');
    }

    public static function subjects()
    {
        return Option::active()->where('key', 'subject');
    }

    protected static function booted(): void
    {
        static::created(function (Contact $contact) {
            AdminNotification::create([
                "title"=> ___('notifications.new_message',),
                "message"=> ___('notifications.new_contact_message_from', ['name' => $contact->name]),
                "type"=> 'contacts',
                "status"=> 'success',
                "link"=> route('dashboard.contacts.show', $contact,false),
            ]);
        });
    }
}
