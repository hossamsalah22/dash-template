<?php

namespace App\Models;

use App\Http\Resources\OptionResource;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Option extends Model implements TranslatableContract
{

    use HasFactory, Translatable;

    protected $guarded = ['id'];


    public static array $keys = [
        'shop_location',
        'sadaka_type',
        'subject'
    ];
    public array $translatedAttributes = ['value'];

    //Scopes
    public function ScopeActive(){
        return $this->where('is_active', 1);
    }


}
