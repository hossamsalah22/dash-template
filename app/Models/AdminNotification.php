<?php

namespace App\Models;

use App\Events\DashboardNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminNotification extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    protected $casts = [
        'title' => 'array',
        'message' => 'array',
        'read_at' => 'datetime',
    ];

    protected $appends = ['title_text', 'message_text'];

    public function getTitleTextAttribute()
    {
        return $this->title[app()->getLocale()];
    }
    public function admin()
    {
        return $this->belongsTo(Admin::class, 'read_by');
    }

    public function getMessageTextAttribute()
    {
        return $this->message[app()->getLocale()];
    }

    public static function booted(): void
    {
        static::created(function (AdminNotification $notification) {
            event(new DashboardNotification(
                id: $notification->id,
                title: $notification->title,
                message: $notification->message,
                type: $notification->type,
                status: $notification->status,
                link: $notification->link,
            ));
        });
    }
}
