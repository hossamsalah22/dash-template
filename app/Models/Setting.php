<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as ContractsTranslatable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Setting extends Model implements ContractsTranslatable , HasMedia
{
    use HasFactory , Translatable , InteractsWithMedia;
    protected $guarded = ['id'];
    public array $translatedAttributes = ['value'];
    public function getValueAttribute()
    {
        if($this->translated){
            return $this->getTranslation(app()->getLocale(),true)?->value;
        }else{
            return $this->getTranslation("en",true)?->value;
        }

    }

    public function getImageAttribute(): string
    {
        return $this->getFirstMediaUrl('settings');
    }

    public function getThumbnailAttribute(): string
    {
        return $this->getFirstMediaUrl('settings', 'preview');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('settings')
            ->singleFile();
    }

    /**
     * @throws InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this
            ->addMediaConversion('preview')
            ->fit(Manipulations::FIT_CROP, 300, 300);
//            ->nonQueued();
    }



}
