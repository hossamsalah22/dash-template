<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Slider extends Model implements HasMedia, TranslatableContract
{
    use HasFactory, InteractsWithMedia, Translatable;

    protected $guarded = ['id'];
    protected array $translatedAttributes = ['title'];
    protected $with = ['translation'];
    //Scopes
    public function ScopeActive()
    {
        return $this->where('is_active', 1);
    }
//Media Library

    public function getImageAttribute(): string
    {
        return $this->getFirstMediaUrl('sliders');
    }

    public function getThumbnailAttribute(): string
    {
        return $this->getFirstMediaUrl('sliders', 'preview');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('sliders')
            ->singleFile();
    }

/**
 * @throws InvalidManipulation
 */
    public function registerMediaConversions(Media $media = null): void
    {
        $this
            ->addMediaConversion('preview')
            ->fit(Manipulations::FIT_CROP, 300, 300);
    }
    public function storeMediaFromRequest()
    {
        $this
            ->addMediaFromRequest('image')
            ->usingFileName($this->id + time() . '.' . request()->file('image')->getClientOriginalExtension())
            ->toMediaCollection('sliders');

    }

    protected static function booted(): void
    {
        static::created(function ($model) {
            if (request()->hasFile('image')) {
                $model->storeMediaFromRequest();
            }
        });

    }
}
