<?php

// Options

use App\Models\Setting;
use Carbon\Carbon;

function ___(string $key, array $data=[]): array
{
    $array = [];
    foreach (config('app.locales') as $locale) {
        $array[$locale] = __($key, $data, $locale);
    }
    return $array;

}

function ____(string $key, array $data=[]): array
{
    $array = [];
    foreach (config('translatable.locales') as $locale) {
        $array[$locale] = __($key, $data, $locale);
    }
    return $array;

}

function getOptions($group)
{
    $options = [
        'shops' => [
            'shop_location' => "fas fa-map-marker-alt",
            'type_of_donation' => "fas fa-comment-dollar",

        ],
        'technical_support' => [
            'subject' => "fas fa-comment-dots",

        ],
        'umrah' => [
            'user_status' => "fas fa-venus-mars",
        ],
    ];

    return $options[$group];
}
//Settings
function getSetting($key , $type='value')
{
    $setting = Setting::where('key', $key)->first();
    if ($setting) {
        return $setting->$type;
    }
}

function settingPages()
{
    return
        [
        'general' => 'fas fa-home',
        'social_links' => 'fab fa-facebook',
        'app_links' => 'fab fa-apple',
        'umrah' => 'fas fa-quran',
        'introduction' => 'fas fa-info-circle',
    ];
}
function formatDate($date)
{
    return  $date?  Carbon::parse($date)->format('Y-m-d H:i:s') : null;
}


