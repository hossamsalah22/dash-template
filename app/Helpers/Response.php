<?php
use Illuminate\Http\JsonResponse;

   function successResponse($message = '' ,$data = [] , $status = 200 ) : JsonResponse
   {

       return response()->json([
           'success'   => true,
           'message'   => $message,
           'data'      => $data
       ] , $status);
   }

   function failedResponse($message = '' , $errors=[], $status = 422) : JsonResponse
   {
       return response()->json([
           'success'   => false,
           'message'   => $message,
           'errors'    => $errors
       ] , $status);
   }
