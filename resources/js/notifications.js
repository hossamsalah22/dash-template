import Echo from 'laravel-echo';
import Pusher from 'pusher-js';

window.Pusher = Pusher;
// console.log(process.env);
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: 'eu',
    forceTLS: false
});
// console.log({
//     key: process.env.MIX_PUSHER_APP_KEY,
//     wsHost: process.env.MIX_PUSHER_HOST,
//     wsPort: process.env.MIX_PUSHER_PORT,
//     wssPort: process.env.MIX_PUSHER_PORT,
// })
// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     wsHost: process.env.MIX_PUSHER_HOST,
//     wsPort: process.env.MIX_PUSHER_PORT,
//     wssPort: process.env.MIX_PUSHER_PORT,
//     forceTLS: false,
//     encrypted: true,
//     disableStats: true,
//     cluster: 'eu',
//     enabledTransports: ['ws', 'wss'],
// });

// console.log(window.Echo);
