@php
$unread_notifications = \App\Models\AdminNotification::latest()->whereNull('read_at')->get();
$unread_notifications_count = $unread_notifications->count();
@endphp


<div class="dropdown d-inline-block">
    <button type="button" class="btn header-item noti-icon waves-effect"
            id="page-header-notifications-dropdown" data-bs-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false" aria-label="notification icon">
        <i class="mdi mdi-bell-outline"></i>
        <span class="badge bg-danger rounded-pill">{{$unread_notifications_count?$unread_notifications_count:""}}</span>
    </button>
    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"
         aria-labelledby="page-header-notifications-dropdown">
        <div class="p-3">
            <div class="row align-items-center">
                <div class="col">
                    <h5 class="m-0 font-size-16"> Notifications (<span id="all_notification_count">{{$unread_notifications->count()}}</span>) </h5>
                </div>
            </div>
        </div>
        <div id="notifications_list" data-simplebar style="max-height: 230px;">
            @foreach($unread_notifications as $notification)
                <a href="{{
                    $notification->link?
                        route('dashboard.notifications.show', $notification->id)
                    :"#"
                }}" data-id="{{$notification->id}}" class="text-reset notification-item">
{{--                <a href="{{$notification->link??"#"}}" onclick="mark_as_read(event)" data-id="{{$notification->id}}" class="text-reset notification-item">--}}
                    <div class="d-flex">
                        <div class="flex-shrink-0 me-3">
                            <div class="avatar-xs">
                                <span class="avatar-title bg-{{$notification->status}} rounded-circle font-size-24">
                                    <i class="mdi mdi-information-variant"></i>
                                </span>
                            </div>
                        </div>
                        <div class="flex-grow-1">
                            <h6 class="mb-1">{{$notification->title_text}}</h6>
                            <div class="font-size-12 text-muted">
                                <p class="mb-1">{{$notification->message_text}}.</p>
                            </div>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
        <div class="p-2 border-top">
            <div class="d-grid">
                <a class="btn btn-sm btn-link font-size-14 text-center" href="{{route('dashboard.notifications.index')}}">
                    View all
                </a>
            </div>
        </div>
    </div>
</div>


<script  src="{{asset('js/notifications.js')}}"></script>
<script>
    function mark_as_read(e){
        e.preventDefault();
        console.log('mark_as_read');
        const notification_id = e.currentTarget.dataset.id;
        console.log('notification_id', notification_id);
        console.log('notification_link', e.currentTarget.href)
        const link = e.currentTarget.href;
        $.ajax({
            type: 'POST',
{{--            url: '{{route('dashboard.notifications.mark_as_read',":id")}}'.replace(':id', notification_id),--}}
            data: {
                _token: '{{csrf_token()}}'
            },
            success: function (response) {
                // Go to notification link
                window.location.href = link;
            },
            error: function (error) {
                console.log('error', error);
            }
        });


    }

    function createNotificationDiv(notification) {
        // create notification node
        const notification_node = document.createElement('a');
        notification_node.href = notification.link ?
            "{{route('dashboard.notifications.show', ":id")}}".replace(':id',notification.id)
            : "#";
        notification_node.className = "text-reset notification-item";
        // notification_node.addEventListener('click', mark_as_read);
        notification_node.dataset.id = notification.id;
        notification_node.innerHTML = `
        <div class="d-flex">
            <div class="flex-shrink-0 me-3">
                <div class="avatar-xs">
                    <span class="avatar-title bg-${notification.status??'info'} rounded-circle font-size-24">
                        <i class="mdi mdi-information-variant"></i>
                    </span>
                </div>
            </div>
            <div class="flex-grow-1">
                <h6 class="mb-1">${notification.title.{{app()->getLocale()}}}</h6>
                <div class="font-size-12 text-muted">
                    <p class="mb-1">${notification.message.{{app()->getLocale()}}}.</p>
                </div>
            </div>
        </div>
    `;
        return notification_node;
    }

    function setNotificationCount(){
        const bill = $('#page-header-notifications-dropdown');
        let notification_count = parseInt(bill.find(".rounded-pill").text());
        notification_count = isNaN(notification_count)?1:notification_count+1;
        bill.find(".rounded-pill").text(notification_count);
        $('#all_notification_count').text(notification_count);
    }

    function prependToNotificationList(notification){
        // Get the notification list
        const notification_list = document.querySelector('#notifications_list');

        // Check if SimpleBar is applied and use the '.simplebar-content' wrapper
        const contentContainer = notification_list.querySelector('.simplebar-content') || notification_list;

        // Create the notification node
        const notification_node = createNotificationDiv(notification);

        // Prepend the notification node
        contentContainer.prepend(notification_node);
    }

    console.log('dashboard-notifications');
    Echo.private('dashboard-notifications')
        .listen('DashboardNotification', (notification) => {
            console.log('listening event', notification);
            setNotificationCount();
            prependToNotificationList(notification);
        })
    ;
</script>

