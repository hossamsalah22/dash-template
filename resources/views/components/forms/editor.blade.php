@props(['name', 'label' => '', 'placeholder' => '', 'required' => true, 'value' => null , 'error_name' => $name])


<div class="col-md-12">

    <label for="{{ $name }}" class="form-label">{{ __('translate.' . $label) }}    @if($required)
        <span class="text-danger">*</span>
    @endif</label>
    <textarea id="{{ $name }}" name="{{ $name }}"  @required($required)> {{ old($error_name, $value) }}</textarea>
    @error($error_name)
        <p class="text-danger small">
            {{ __($message) }}

        </p>
    @enderror
</div>



@push('scripts')
    <!--tinymce js-->
    <script src="{{ URL::asset('assets/libs/tinymce/tinymce.min.js') }}"></script>


    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
    <script>
        $(document).ready(function() {
            var editor = document.getElementById("{{ $name }}")
            if (editor) {
                tinymce.init({
                    target: editor,
                    height: 300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                    style_formats: [{
                            title: 'Bold text',
                            inline: 'b'
                        },
                        {
                            title: 'Red text',
                            inline: 'span',
                            styles: {
                                color: '#ff0000'
                            }
                        },
                        {
                            title: 'Red header',
                            block: 'h1',
                            styles: {
                                color: '#ff0000'
                            }
                        },
                        {
                            title: 'Example 1',
                            inline: 'span',
                            classes: 'example1'
                        },
                        {
                            title: 'Example 2',
                            inline: 'span',
                            classes: 'example2'
                        },
                        {
                            title: 'Table styles'
                        },
                        {
                            title: 'Table row 1',
                            selector: 'tr',
                            classes: 'tablerow1'
                        }
                    ]
                });
            }



        });
    </script>
@endpush
