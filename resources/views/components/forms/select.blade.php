@props([
    'name',
    'label',
    'placeholder' => '',
    'value' => [], // [0=>1,1=>2]
    'multiple' => false,
    'required' => true,
    'error_name' => $name,
    'options' => [],
])

<div class="col-md-12">
    <label class="form-label">
        {{ __('translate.' . $label) }}
        @if($required)
            <span class="text-danger">*</span>
        @endif
    </label>
    <select class="form-control  {{ $multiple ? 'select2-multiple' : 'select2' }}" name="{{ $name }}"
        @required($required) {{ $multiple ? 'multiple' : '' }}
    >
        @if($placeholder)
            <option disabled selected value="">{{ __('translate.' . $placeholder) }}</option>
        @endif
        @foreach ($options as $key => $option)
            <option value="{{ $key }}"
                @if($multiple)
                    {{ in_array($key, old($error_name, $value ?? [])) ? 'selected' : '' }}
                @else
                    {{ old($error_name, $value) == $key ? 'selected' : '' }}
                @endif
                >{{ $option }}</option>
        @endforeach
    </select>
    @error($error_name)
    <p class="text-danger small">
        {{ __($message) }}
    </p>
    @enderror
</div>
