@props(['name', 'placeholder' => '', 'label' => '', 'value' => 0])

<div class="col-md-12">
    <input class="form-check form-switch" name="{{ $name }}" type="checkbox" id="{{ $name }}"
        @checked($value) value="1" switch="none"  >

    <label class="form-label" for="{{ $name }}" data-on-label="On" data-off-label="Off"></label>
</div>
