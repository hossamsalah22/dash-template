@props([
    'name' => 'file',
    'placeholder' => '',
    'label' => null,
    'required' => true,
    'multiple' => false,
    'path' => '',
    'error_name' => $name
])

@if($label)
    <label for="{{ $name }}" class="form-label">{{ __('translate.' . $label) }} @if($required)
            <span class="text-danger">*</span>
        @endif</label>

@endif
<div class="col-md-12 dropzone ">
    <div class="fallback">
        <input name="{{$name}}" type="file" {{ $multiple ? 'multiple' : '' }} @required($required)>
    </div>

    <div class="dz-message needsclick">
        @if ($path)
            <img src="{{ asset($path) }}" alt="image" class="img-thumbnail" width="100" height="100">
        @endif
        <div class="mb-3">
            <i class="mdi mdi-cloud-upload display-4 text-muted"></i>
        </div>
        <h4>@lang('translate.drop_files_here_or_click_to_upload')</h4>

    </div>
    @error($error_name)
        <p class="text-danger small">
            {{ __($message) }}

        </p>
    @enderror
</div>
@push('scripts')
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>

    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
@endpush
