@props([
    'name',
    'label',
    'required' => true,
    'value' => null ,
    'error_name' => $name,
])
<div class="col-md-12 mb-1">
    <label for="{{ $name }}" class="form-label">
        {{ __('translate.' . $label) }}

        @if($required)
            <span class="text-danger">*</span>
        @endif

    </label>
    <textarea class="form-control"
           id="{{ $name }}" name="{{ $name }}"
        @required($required)
        {{$attributes}}
    >{{ old($error_name, $value) }}</textarea>

    @error($error_name)
    <p class="text-danger small">
        {{ __($message) }}
    </p>
    @enderror
</div>
