@props([
    'value'
])
<div class="mt-4">
    <button type="submit" class="btn btn-primary w-md">{{__('translate.' . $value)}}</button>
</div>
