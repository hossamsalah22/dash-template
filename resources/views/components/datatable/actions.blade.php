@props([
    'id',
    'route',
    'edit'=>1,
    'delete'=>1,
    'show'=>0,
    'param'=>null
])
@if($param == null)
    @php
        $param = $id;
    @endphp
@else
    @php
        $param = [$param, $id];
    @endphp
@endif
<div style="width: 100%" class="text-center justify-content-evenly d-flex">
    @if($show)
        <a class="btn btn-success btn-sm show" title="Show" href="{{ route("dashboard.{$route}.show", $param??$id ) }}">
            <i class="fas fa-eye"></i>
        </a>
    @endif

    @if($edit)
        <a class="btn btn-success btn-sm edit" title="Edit" href="{{ route("dashboard.{$route}.edit", $param) }}">
            <i class="fas fa-pencil-alt"></i>
        </a>
    @endif

    @if($delete)
        <button type="button" class="btn btn-danger btn-sm delete" title="Delete" data-bs-toggle="modal"
{{--                data-bs-target="#staticBackdrop"--}}
                data-id="{{$id}}"
                onclick="openModal({{$id}},'{{$route}}')"
        >
            <i class="fas fa-trash"></i>
        </button>
    @endif
</div>



