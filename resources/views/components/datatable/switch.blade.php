@props([$id,$active])
    <input type="checkbox" class="form-check form-switch toggle-is-active" switch="none" id="{{$id}}"  data-property="is_active" data-id="{{$id}}" @checked($active)>
    <label class="form-label" for="{{$id}}" data-on-label="@lang('translate.yes')" data-off-label="@lang('translate.no')"></label>



{{--
//TODO DON'T FORGET TO ADD THIS TO THE END OF THE DATATABLE INDEX FILE
<script>
     $('#datatable').on('change', '.toggle-is-active', function() {
            var recordId = $(this).data('id');
            var isActive = $(this).prop('checked');
            var property = $(this).data('property');
            var value = isActive ? 1 : 0;
            const noty = new Noty({
                timeout: 2000,
            });
            $.ajax({
                url: "{{ route("dashboard.{$route}.toggle", ':recordId') }}".replace(':recordId', recordId),
                type: "POST",
                data: {
                    property: property,
                    value: value,
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    noty.push({
                        text: response.message,
                        type: 'success',
                    });

                },
                error: function(error) {
                    console.log(error);
                    noty.push({
                        text: error.responseJSON.message,
                        type: 'error',
                    });
                }
            });
        });

</script>--}}
