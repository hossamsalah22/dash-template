<div class="modal fade" id="deleteModal" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalTitle"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                {{ __('translate.delete_confirmation') }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{__("translate.close")}}</button>
                <button type="button" class="btn btn-danger" id="deleteButton">{{__("translate.delete")}}</button>

            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
<div class="modal fade" id="responseModal" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1"
     aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="responseTitle"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
{{--            <div class="modal-body">--}}
{{--                {{ __('translate.delete_confirmation') }}--}}
{{--            </div>--}}
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{__("translate.close")}}</button>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->




@push('scripts')
    <script>
        function deleteItem(id) {
            $.ajax({
                url: "{{ route("dashboard.{$route}.destroy", ':id') }}".replace(':id', id),
                type: "DELETE",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function (response) {
                    $('#staticBackdrop-' + id).modal('hide');
                    $('#datatable').DataTable().ajax.reload();
                    $('#deleteModal').modal('hide');
                    const successModal = $('#responseModal');
                    $('#responseTitle').text(response.message);
                    successModal.modal('show');
                },
                error: function (error) {
                    console.log(error);
                    const successModal = $('#responseModal');
                    $('#modalTitle').text(`@lang('translate.success')`);
                    $('#responseModal .modal-body').text(error.message);
                    successModal.modal('show');
                }
            });
        }
        function openModal(id) {
            const modal = $('#deleteModal');
            const singularRoute = "{{Str::singular($route)}}"; // Assuming you have a way to get the singular form of the route in JavaScript

            // Set the title
            {{--$('#modalTitle').text(`@lang('translate.delete') ${singularRoute} #${id}`);--}}
            $('#modalTitle').text(`@lang('translate.delete') ${singularRoute}`);

            // Update the delete button's onclick attribute
            $('#deleteButton').attr('onclick', `deleteItem(${id})`);

            modal.modal('show');
        }
    </script>
@endpush
