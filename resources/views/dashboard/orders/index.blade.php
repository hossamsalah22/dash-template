@extends('dashboard.layouts.master')

@php($route = 'orders' )

@section('title', __('translate.' . $route))

@section('breadcrumbs', Breadcrumbs::render('resource'))

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}">
@endsection
@inject('orders', 'App\Models\Order')




@section('content')

<div class="row">
    @foreach ($statuses as $status=>$icon)
    <div class="col-xl-3 col-md-6">
        <a href="{{ route('dashboard.orders.index', ['filter[status]' => $status]) }}">
                    <div class="card mini-stat text-white card-filter">
            <div class="card-body">
                <div class="mb-4">
                    <div class="float-start mini-stat-img me-4">
                        <i class="{{ $icon }}"></i>
                    </div>
                    <h5 class="font-size-16 text-uppercase text-white-50">@lang('translate.' . $status)</h5>
                    <h4 class="fw-medium font-size-24">{{ $orders->where('status', $status)->count() }}</h4>

                </div>

            </div>
        </div>
    </a>
    </div>
    @endforeach


</div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">


                    <table id="datatable" class="table table-bordered dt-responsive nowrap"
                        style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th> @lang('translate.order_number')</th>
                                <th> @lang('translate.user')</th>
                                <th> @lang('translate.items_count')</th>
                                <th> @lang('translate.coupon')</th>
                                <th> @lang('translate.donation_type')</th>
                                <th> @lang('translate.subtotal')</th>
                                <th> @lang('translate.discount')</th>
                                <th> @lang('translate.total')</th>
                                <th> @lang('translate.status')</th>
                                <th> @lang('translate.created_at')</th>
                                <th> @lang('translate.action')</th>

                            </tr>
                        </thead>


                        <tbody>

                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection


@push('scripts')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/libs/datatables/datatables.min.js') }}"></script>

    <!-- Datatable init js -->
    {{-- <script src="{{URL::asset('assets/js/pages/datatables.init.js')}}"></script> --}}

    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
    <script>

let datatableUrl = '{{ route("dashboard." . $route . ".index", ["filter" => $filter]) }}';
    </script>
    <script>
        $(document).ready(function() {

            let table = $('#datatable').DataTable({
                "language": {
                    "url": $('#importLangLocal').attr('data-LangLocal'),

                },
                serverSide: true,
                ajax: {
                    url: datatableUrl,
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false,
                        orderable: false
                    },
                    {
                        data: 'order_number',
                        name: 'order_number',
                        searchable: true,
                        sortable: true
                    },

                    {
                        data: 'user.name',
                        name: 'user.name',
                        searchable: true,
                        sortable: true,
                        render: function(data, type, row) {
                            return `<a href="{{ route('dashboard.users.show', ':id') }}">${data}</a>`
                                .replace(':id', row.user.id);
                        }
                    },

                    {
                        data: 'items_count',
                        name: 'items_count',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data:'coupon.coupon',
                        name:'coupon.coupon',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data:'donation_type.value',
                        name:'donation_type.value',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'subtotal',
                        name: 'subtotal',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'discount',
                        name: 'discount',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'total_price',
                        name: 'total',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'status',
                        name: 'status',
                        searchable: true,
                        sortable: true,


                    },

                    {
                        data: 'created_at',
                        name: 'created_at',
                        searchable: false
                    },

                    {
                        data: 'actions',
                        name: 'actions',
                        searchable: false,
                        sortable: false
                    }
                ]
            });
        });
    </script>
@endpush
