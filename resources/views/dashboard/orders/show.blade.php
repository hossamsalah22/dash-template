@extends('dashboard.layouts.master')

@php($route = 'orders')

@section('title', __('translate.show') . ' ' . __('translate.' . Str::singular($route)))

@section('breadcrumbs', Breadcrumbs::render('resource'))
@section('action')
    <button type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#myModal">
        @lang('translate.change_status')</button>
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel"> @lang('translate.change_status')
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('dashboard.orders.update', $order->id) }}" method="POST">
                    <div class="modal-body">
                        @csrf
                        @method('PUT')
                        <x-forms.select label="status" name="status" :options=$statuses placeholder="select_status"
                            error_name="status" />


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect"
                            data-bs-dismiss="modal">@lang('translate.close')</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">@lang('translate.update')
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('content')

    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <table class="table">
                        <tbody>
                            <tr class="row">
                                <th class="col-3">{{ __('translate.order_number') }}</td>
                                <td class="col-9">{{ $order->order_number }} <span
                                        class="badge bg-{{ $order->getStatusColor() }}">{{ __('translate.' . $order->status) }}</span>
                                </td>
                                <th class="col-3">{{ __('translate.user') }}</td>
                                <td class="col-9"><a
                                        href="{{ route('dashboard.users.show', $order->user->id) }}">{{ $order->user->name }} </a>
                                </td>
                                <th class="col-3">{{ __('translate.phone') }}</td>
                                <td class="col-9">{{ $order->user->phone }}</td>
                                <th class="col-3">{{ __('translate.subtotal') }}</td>

                                <td class="col-9">{{ $order->subtotal }}</td>
                                <th class="col-3">{{ __('translate.discount') }}</td>
                                <td class="col-9">{{ $order->discount }} </td>
                                <th class="col-3">{{ __('translate.coupon') }}</td>

                                <td class="col-9">{{ $order->coupon?->coupon }}</td>
                                <th class="col-3">{{ __('translate.total') }}</td>
                                <td class="col-9">{{ $order->total_price }}</td>
                                <th class="col-3">{{ __('translate.type') }}</td>

                                <td class="col-9">{{ $order->donationType->value }}</td>
                                <th class="col-3">{{ __('translate.created_at') }}</td>

                                <td class="col-9">{{ $order->created_at }}</td>
                                <th class="col-3">{{ __('translate.updated_at') }}</td>

                                <td class="col-9">{{ $order->updated_at }}</td>


                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">{{ __('translate.order_items') }}</h4>


                    <div class="table-responsive">
                        <table class="table mb-0 table-bordered">

                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@Lang('translate.name')</th>
                                    <th>@Lang('translate.quantity')</th>
                                    <th>@Lang('translate.price')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($order->items as $item)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ $item->shopItem->name }}</td>
                                        <td>{{ $item->quantity }}</td>
                                        <td>{{ $item->price }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <!-- end tbody -->
                        </table><!-- end table -->
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">{{ __('translate.order_log') }}</h4>
                    <ol class="activity-feed">
                        @foreach ($order->history->reverse() as $history)
                            <li class="feed-item">
                                <div class="feed-item-list">
                                    <span class="date">{{ $history->created_at->format('d-m-Y H:i:s') }}</span>
                                    <span class="activity-text">{{ __('translate.' . $history->status) }}</span>
                                </div>
                            </li>
                        @endforeach

                    </ol>

                </div>
            </div>
        </div>
    </div> <!-- end row -->





@endsection
