@extends('dashboard.layouts.master')

@php($route = 'settings')
@section('title', __('translate.create') . ' ' . __('translate.' . $page))

@section('breadcrumbs', Breadcrumbs::render('resource', __('translate.' . $page)))

@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form class="row g-3" action="{{ route("dashboard.{$route}.store") }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @if($options->where('translated', 1)->count() > 0)
                    @foreach (config('translatable.locales') as $locale)
                            <h5 class="mb-4">{{ __("translate.$locale.$locale") }}</h5>
                            @foreach ($options as $option)
                                @if ($option->translated == 1)
                                    @if ($option->type == 'text' || $option->type == 'email')
                                        <x-forms.input label="{{ $option->key }}" :required="false" value="{{ $option->translate($locale)?->value }}"
                                            name="{{ $option->key . '[' . $locale . ']' .'[value]'  }}" type="{{ $option->type }}" />
                                    @elseif($option->type == 'editor')
                                        <x-forms.editor label="{{ $option->key }}" :required="false" value="{{ $option->value }}"
                                            name="{{ $option->key . '[' . $locale . ']' .'[value]'  }}" value="{{ $option->translate($locale)?->value }}" />
                                    @elseif($option->type == 'textarea')
                                    <x-forms.textarea label="{{ $option->key }}" name="{{ $option->key . '[' . $locale . ']' .'[value]'  }}" type="text"
                                        value="{{ $option->translate($locale)?->value }}"
                                         :required=0 />

                                    @endif


                                @endif
                            @endforeach
                    @endforeach
                    @endif

                    @foreach ($options as $option)
                        @if ($option->translated == 0)
                            @if ($option->type == 'text' || $option->type == 'email')
                                <x-forms.input label="{{ $option->key }}" :required="false" name="{{ $option->key . '[value]' }}"
                                    type="{{ $option->type }}" value="{{ $option->value }}" />
                            @elseif($option->type == 'number')
                                <x-forms.input label="{{ $option->key}}" :required="false"
                                    name="{{ $option->key . '[value]' }}" type="number" min="1" value="{{ $option->value }}" />
                            @elseif($option->type == 'image')
                                <x-forms.uploadFile name="{{ $option->key}}[image]" label="{{ $option->key }}" label="{{ $option->key }}"
                                    :required="false" path="{{ $option->thumbnail }}" />
                            @endif
                        @endif
                    @endforeach
                    <x-forms.submit value="save" />
                </form>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->




@endsection
