@extends('dashboard.layouts.master')
@section('title')
    @lang('translate.edit') @lang('translate.' . $key)
@endsection

@section('breadcrumbs', Breadcrumbs::render('options.edit', $key, $option->id))

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="row g-3" action="{{ route('dashboard.key.options.update', [$key, $option->id]) }}"
                        method="post">
                        @csrf
                        @method('PUT')
                        @foreach (config('translatable.locales') as $locale)
                            <x-forms.input label="{{ $locale . '.name' }}" name="{{ $locale . '[value]' }}" type="text"
                                value="{{ $option->translate($locale)->value }}" error_name="{{ $locale . '.value' }}" />
                        @endforeach
                        <x-forms.submit value="update" />
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
