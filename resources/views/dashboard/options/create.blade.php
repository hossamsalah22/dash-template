@extends('dashboard.layouts.master')
@section('title')
  @lang('translate.create')  @lang('translate.' . $key)
@endsection
@section('breadcrumbs', Breadcrumbs::render('options.create' , $key))
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="row g-3 " action="{{ route('dashboard.key.options.store' , $key) }}" method="post">
                        @csrf
                        @foreach (config('translatable.locales') as $locale)
                            <x-forms.input label="{{ $locale . '.name' }}" name="{{ $locale . '[value]' }}" type="text" error_name="{{ $locale . '.value' }}" />
                        @endforeach
                        <x-forms.submit value="create" />
                    </form>


                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

