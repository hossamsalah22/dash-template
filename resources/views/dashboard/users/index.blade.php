@extends('dashboard.layouts.master')

@php($route='users')

@section('title', __('translate.'.$route))

@section('breadcrumbs', Breadcrumbs::render('resource'))

@section('css')
    /*<!-- DataTables -->*/
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}">
@endsection

{{--@section('action')--}}
{{--    <button class="btn btn-primary  dropdown-toggle" type="button"--}}
{{--            onclick="location.href='{{ route("dashboard.{$route}.create") }}'">--}}
{{--        <i class="mdi mdi-plus"></i> @lang('translate.create')--}}
{{--    </button>--}}
{{--@endsection--}}

<x-datatable.delete :$route/>


@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">


                    <table id="datatable" class="table table-bordered dt-responsive nowrap"
                           style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th> @lang('translate.name')</th>
                            <th> @lang('translate.email')</th>
                            <th> @lang('translate.phone')</th>
                            <th> @lang('translate.completed')</th>
                            <th> @lang('translate.umrah_active')</th>
                            <th> @lang('translate.orders')</th>
                            <th> @lang('translate.is_active')</th>
                            <th> @lang('translate.created_at')</th>
                            <th> @lang('translate.action')</th>

                        </tr>
                        </thead>


                        <tbody>

                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@push('scripts')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/libs/datatables/datatables.min.js') }}"></script>

    <!-- Datatable init js -->
    {{-- <script src="{{URL::asset('assets/js/pages/datatables.init.js')}}"></script> --}}

    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
    <script>
        let datatableUrl = '{{ route("dashboard.{$route}.index") }}';
    </script>
    <script>
        $(document).ready(function() {

            let table = $('#datatable').DataTable({
                "language": {
                    "url": $('#importLangLocal').attr('data-LangLocal'),

                },
                serverSide: true,
                ajax: {
                    url: datatableUrl,
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false, orderable: false },

                    {
                        data: 'name',
                        name: 'name',
                        searchable: true,
                        sortable: true
                    }, {
                        data: 'email',
                        name: 'email',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'phone',
                        name: 'phone',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'completed_ummrah_count',
                        name: 'completed_ummrah_count',
                        searchable: false,

                        sortable: true
                    },
                    {
                        data: 'active_ummrah_count',
                        name: 'active_ummrah_count',
                        searchable: false,

                        sortable: true
                    },


                    {
                        data: 'orders_count',
                        name: 'orders_count',
                        searchable: false,

                        sortable: true
                    },

                    {
                        data: 'is_active',
                        name: 'is_active',
        //                 render: function(data, type, full, meta) {
        //                     return `
        // <div class="form-check form-switch">
        //     <input type="checkbox" class="form-check-input toggle-is-active"  data-property="is_active" data-id="${full.id}" ${data == 1 ? 'checked' : ''}>
        // </div>`;
        //                 },

                        searchable: true,
                        sortable: true
                    },

                    {
                        data: 'created_at',
                        name: 'created_at',
                        searchable: false
                    },
                    {
                        data: 'actions',
                        name: 'actions',
                        searchable: false,
                        sortable: false
                    }
                ],

            });
        });

        $('#datatable').on('change', '.toggle-is-active', function() {
            var recordId = $(this).data('id');
            var isActive = $(this).prop('checked');
            var property = $(this).data('property');
            var value = isActive ? 1 : 0;
            const noty = new Noty({
                timeout: 2000,
            });
            $.ajax({
                url: "{{ route("dashboard.{$route}.toggle", ':recordId') }}".replace(':recordId', recordId),
                type: "POST",
                data: {
                    property: property,
                    value: value,
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    noty.push({
                        text: response.message,
                        type: 'success',
                    });

                },
                error: function(error) {
                    console.log(error);
                    noty.push({
                        text: error.responseJSON.message,
                        type: 'error',
                    });
                }
            });
        });

    </script>
@endpush



