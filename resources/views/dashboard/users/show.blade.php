@extends('dashboard.layouts.master')

@php($route='agents')

@section('title', __('translate.show') ." "  . __('translate.'.Str::singular($route) ))

@section('breadcrumbs', Breadcrumbs::render('resource'))

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table">
                        <tbody>
                        @foreach($user->toArray() as $key=>$value)
                            @if(in_array($key,['id','email_verified_at','created_at','updated_at']))
                                @continue
                            @endif
                            <tr class="row">
                                <td class="col-3">{{__("translate.".$key)}}</td>
                                <td  class="col-9">
                                @if(in_array($key,['image','avatar','passport_image']))
                                    <img src="{{asset($value)}}" alt="{{$key}}" class="img-thumbnail">
                                @else
                                    {{$value}}

                                @endif
                                </td>
                            </tr>
                        @endforeach
                        <tr class="row">
                            <td class="col-3">
                                <form action="{{ route('dashboard.users.toggle',$user->id) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-{{!$user->is_active?"success":"danger"}}">
                                        @if(!$user->is_active)
                                            <i class="mdi mdi-reply"></i> @lang('translate.activate')
                                        @else
                                            <i class="mdi mdi-reply"></i> @lang('translate.deactivate')
                                        @endif
                                    </button>
                                </form>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
