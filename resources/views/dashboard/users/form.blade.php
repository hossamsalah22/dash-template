@extends('dashboard.layouts.master')

@section('title', 'Users')

@section('breadcrumbs', Breadcrumbs::render('resource'))

@section('action')
    <button class="btn btn-primary  dropdown-toggle" type="button"
            onclick="location.href='{{ route('dashboard.shops.create') }}'">
        <i class="mdi mdi-plus"></i> @lang('translate.create')
    </button>
@endsection

@section("content")

    @if(isset($admin))
        <livewire:users.form :$user />
    @else
        <livewire:users.form  />
    @endif

@endsection
