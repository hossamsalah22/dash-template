@yield('css')
<!-- Bootstrap Css -->
<!-- Icons Css -->
<!-- App Css-->
@if(app()->getLocale() == 'ar')
<link href="{{URL::asset('assets/css/bootstrap.rtl.css')}}" id="bootstrap-style" rel="stylesheet" type="text/css">
<link href="{{URL::asset('assets/css/icons.rtl.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('assets/css/app.rtl.css')}}" id="app-style" rel="stylesheet" type="text/css">
@else
<link href="{{URL::asset('assets/css/bootstrap.min.css')}}" id="bootstrap-style" rel="stylesheet" type="text/css">
<link href="{{URL::asset('assets/css/icons.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('assets/css/app.min.css')}}" id="app-style" rel="stylesheet" type="text/css">
@endif
<link href="{{URL::asset('assets/libs/chartist/chartist.min.css')}}" rel="stylesheet">
<link href="{{URL::asset('assets/libs/dropzone/dropzone.min.css')}}" rel="stylesheet">
