<!-- start page title -->
<div class="page-title-box">
    <div class="row align-items-center">
        <div class="col-md-8">
            <h6 class="page-title">@yield('title')</h6>
            <ol class="breadcrumb m-0">
                @foreach($breadcrumbs as $breadcrumb)
                  @if($breadcrumb->url && !$loop->last)
                        <li class="breadcrumb-item active" aria-current="page"><a
                                href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a> </li>
                    @else
                        <li class="breadcrumb-item active" aria-current="page">{{ $breadcrumb->title }}</li>
                    @endif
                @endforeach
            </ol>
        </div>
        <div class="col-md-4">
            <div class="float-end d-none d-md-block">
                @yield('action')
            </div>
        </div>
    </div>
</div>
<!-- end page title -->
