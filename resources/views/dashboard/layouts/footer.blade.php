
<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
             @lang('translate.all_rights_reserved') {{getSetting('app_name')}}  &copy; {{date('Y')}} @lang('translate.developed_by') <a href="https://code7x.sa" target="_blank" >
                 Code7X </a>

            </div>
        </div>
    </div>
</footer>
