<header id="page-topbar">
    <div class="navbar-header">
        <div class="d-flex">
            <!-- LOGO -->
            <div class="navbar-brand-box">
                <a href="index" class="logo logo-dark">
                    <span class="logo-sm">
                        <img src="{{getSetting('dashboard_logo' , 'image')}}" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="{{getSetting('dashboard_logo' , 'image')}}" alt="" height="17">
                    </span>
                </a>

                <a href="index" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="{{getSetting('dashboard_logo' , 'image')}}" alt="" height="22">
                    </span>
                    <span class="logo-lg">
                        <img src="{{getSetting('dashboard_logo' , 'image')}}" alt="" height="18">
                    </span>
                </a>
            </div>

            <button type="button" class="btn btn-sm px-3 font-size-24 header-item waves-effect" id="vertical-menu-btn"
                aria-label="Vertical Menu Button">
                <i class="mdi mdi-menu"></i>
            </button>

{{--            <div class="d-none d-sm-block">--}}
{{--                <div class="dropdown pt-3 d-inline-block">--}}
{{--                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink"--}}
{{--                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                        Create <i class="mdi mdi-chevron-down"></i>--}}
{{--                    </a>--}}

{{--                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">--}}
{{--                        <a class="dropdown-item" href="#">Action</a>--}}
{{--                        <a class="dropdown-item" href="#">Another action</a>--}}
{{--                        <a class="dropdown-item" href="#">Something else here</a>--}}
{{--                        <div class="dropdown-divider"></div>--}}
{{--                        <a class="dropdown-item" href="#">Separated link</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>

        <div class="d-flex">
            <!-- App Search-->
{{--            <form class="app-search d-none d-lg-block">--}}
{{--                <div class="position-relative">--}}
{{--                    <input type="text" class="form-control" placeholder="Search...">--}}
{{--                    <span class="fa fa-search"></span>--}}
{{--                </div>--}}
{{--            </form>--}}

{{--            <div class="dropdown d-inline-block d-lg-none ms-2">--}}
{{--                <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown"--}}
{{--                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="notification icon">--}}
{{--                    <i class="mdi mdi-magnify"></i>--}}
{{--                </button>--}}
{{--                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"--}}
{{--                    aria-labelledby="page-header-search-dropdown">--}}

{{--                    <form class="p-3">--}}
{{--                        <div class="form-group m-0">--}}
{{--                            <div class="input-group">--}}
{{--                                <input type="text" class="form-control" placeholder="Search ..."--}}
{{--                                    aria-label="Recipient's username">--}}
{{--                                <div class="input-group-append">--}}
{{--                                    <button class="btn btn-primary" type="submit"><i--}}
{{--                                            class="mdi mdi-magnify"></i></button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}

            <div class="dropdown d-none d-md-block ms-2">
                <button type="button" class="btn header-item waves-effect" data-bs-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false" aria-label="language button">
                    <img class="me-2" src="{{ URL::asset('assets/images/flags/' . LaravelLocalization::getCurrentLocale().'_flag.jpg') }}" alt="Header Language"
                        height="16"> {{ LaravelLocalization::getCurrentLocaleNative() }} <span class="mdi mdi-chevron-down"></span>
                </button>
                <div class="dropdown-menu dropdown-menu-end">
                    @foreach (config('app.locales') as $locale)
                        <a href="{{ LaravelLocalization::getLocalizedURL($locale, null, [], false) }}"
                            class="dropdown-item notify-item">
                            <img src="{{ URL::asset('assets/images/flags/' . $locale . '_flag.jpg') }}" alt="user-image"
                                class="me-1" height="12"> <span class="align-middle">
                                {{ LaravelLocalization::getSupportedLocales()[$locale]['native'] }}
                            </span>
                        </a>
                    @endforeach
                </div>


            </div>

{{--            <div class="dropdown d-none d-lg-inline-block">--}}
{{--                <button type="button" class="btn header-item noti-icon waves-effect" data-bs-toggle="fullscreen"--}}
{{--                    aria-label="fullscreen button">--}}
{{--                    <i class="mdi mdi-fullscreen"></i>--}}
{{--                </button>--}}
{{--            </div>--}}

            @include('components.dashboard.notifications')

            <div class="dropdown d-inline-block">
                <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                    data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" aria-label="user dropdown">
                    <img class="rounded-circle header-profile-user"
                        src="{{ URL::asset('assets/images/users/user-4.jpg') }}" alt="Header Avatar">
                </button>
                <div class="dropdown-menu dropdown-menu-end">
                    <!-- item-->
                    <a class="dropdown-item" href="{{route("dashboard.admins.edit",auth()->id())}}"><i
                            class="mdi mdi-account-circle font-size-17 align-middle me-1"></i> Profile</a>
                    {{--<a class="dropdown-item" href="#"><i
                            class="mdi mdi-wallet font-size-17 align-middle me-1"></i> My Wallet</a>
                    <a class="dropdown-item d-flex align-items-center" href="#"><i
                            class="mdi mdi-cog font-size-17 align-middle me-1"></i> Settings<span
                            class="badge bg-success ms-auto">11</span></a>
                    <a class="dropdown-item" href="#"><i
                            class="mdi mdi-lock-open-outline font-size-17 align-middle me-1"></i> Lock screen</a>
                    <div class="dropdown-divider"></div>
                    --}}
                    <a class="dropdown-item text-danger" href="{{ route('dashboard.logout') }}"
                        onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i
                            class="bx bx-power-off font-size-17 align-middle me-1 text-danger"></i>
                        {{ __('Logout') }} </a>
                    <form id="logout-form" action="{{ route('dashboard.logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </div>



        </div>
    </div>
</header>
