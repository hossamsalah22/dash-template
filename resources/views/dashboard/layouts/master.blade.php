<!doctype html>
<html lang="{{app()->getLocale()}}" dir={{app()->getLocale() == 'ar' ? 'rtl' : 'ltr'}}>
<head>
    <meta charset="utf-8">

    <title> @yield('title') | {{getSetting('app_name')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description">
    <meta name="keywords" content="veltrix,veltrix laravel,admin template,new admin panel,laravel 10">
    <meta content="Themesbrand" name="author">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ URL::asset(getSetting('favicon','image')) }}">
    <!-- Noty Css -->
    <link href="{{ URL::asset('assets/css/noty.min.css') }}" rel="stylesheet" type="text/css">

    @include('dashboard.layouts.head-css')
    <style>
        #datatable * {
            text-align: center;
        }
         .card-filter {
            background-color:#525f80;
        }
    </style>


</head>

<body data-sidebar="dark">
    <!-- Begin page -->
    <div id="layout-wrapper">

        @include('dashboard.layouts.topbar')
        @include('dashboard.layouts.sidebar')

        <!-- Begin page -->
        <div class="page-wrapper">

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">
                    <div class="container-fluid">
                        @yield('breadcrumbs')
                        @include('dashboard.layouts.alerts')
                        @yield('content')
                    </div>
                    <!-- end container -->
                </div>
                <!-- end page content -->
                @include('dashboard.layouts.footer')
            </div>
            <!-- end main content -->
        </div>
        <!-- END wrapper -->
        <div id="importLangLocal"
             data-LangLocal="{{ asset('datatable-lang/' . app()->getLocale() . '.json') }}"></div> {{-- for data table lang --}}

    </div>
    <!-- script file here -->
    @include('dashboard.layouts.vendor-scripts')
    <script src="{{ asset('assets/js/noty.min.js') }}"></script>
</body>

</html>
