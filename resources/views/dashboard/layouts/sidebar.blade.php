<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">

                <li class="menu-title">@lang('translate.technical_support')</li>
                @foreach (getOptions('technical_support') as $key => $value)
                    <li>
                        <a href="{{ route('dashboard.key.options.index', $key) }}" class="waves-effect">
                            <i class="{{ $value }}"></i>
                            <span>@lang('translate.' . $key . 's')</span>
                        </a>
                    </li>
                @endforeach
                <li>
                    <a href="{{ route('dashboard.contacts.index') }}" class="waves-effect">
                        <i class="fas fa-phone"></i>
                        @php($contacts = \App\Models\Contact::where('is_read', 0)->count())

                        @if($contacts > 0)
                            <span class="badge rounded-pill bg-danger float-end">
                            {{ $contacts }}
                        </span>
                        @endif
                        <span>@lang('translate.contacts')</span>
                    </a>
                </li>

                <li class="menu-title">@lang('translate.users')</li>

                <li>
                    <a href="{{ route('dashboard.admins.index') }}" class="waves-effect">
                        <i class="fas fa-user-shield"></i>
                        <span>@lang('translate.admins')</span>
                    </a>
                </li>




                <li class="menu-title">@lang('translate.settings')</li>

                @foreach (settingPages() as $key => $value)
                    <li>
                        <a href="{{ route('dashboard.settings.create', ['page' => $key]) }}" class="waves-effect">
                            <i class="{{ $value }}"></i>
                            <span>@lang('translate.' . $key)</span>
                        </a>
                    </li>
                @endforeach
                <li>
                    <a href="{{ route('dashboard.pages.index') }}" class="waves-effect">
                        <i class="fas fa-file
                    "></i>
                        <span>@lang('translate.pages')</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('dashboard.sliders.index') }}" class="waves-effect">
                        <i class="fas fa-image
                    "></i>
                        <span>@lang('translate.sliders')</span>
                    </a>
                </li>









            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->
