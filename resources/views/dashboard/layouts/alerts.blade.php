@if(session()->has('message'))
<div class="alert alert-info" role="alert">
    {{ session('message') }}
    </div>
@endif
@if(session()->has('success'))
<div class="alert alert-success" role="alert">
    {{ session('success') }}
    </div>
@endif
@if(session()->has('warning'))
    <div class="alert alert-warning">
        {{ session('warning') }}
    </div>
@endif
@if(session()->has('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif

