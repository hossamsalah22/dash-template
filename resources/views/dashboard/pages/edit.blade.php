@extends('dashboard.layouts.master')

@php($route='pages')

@section('title', __('translate.edit')  . __('translate.'.Str::singular($route) ))

@section('breadcrumbs', Breadcrumbs::render('resource'))

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-4"> @lang('translate.edit')  @lang('translate.'.Str::singular($route)) </h4>
                    <form class="row g-3 " action="{{route("dashboard.{$route}.update",$page->id)}}" method="post" >
                        @csrf
                        @method('PUT')
                        @foreach (config('translatable.locales') as $locale)
                            <h5 class="mt-3 mb-0">{{ __("translate.$locale.$locale")}}</h5>
                            <x-forms.input
                                label="{{ 'title' }}" name="{{ $locale . '[title]' }}"
                                type="text" error_name="{{ $locale . '.title' }}"
                                :value="$page->translate($locale)->title"
                            />
                            <x-forms.textarea
                                label="{{ 'content' }}" name="{{ $locale . '[content]' }}"
                                error_name="{{ $locale . '.content' }}" :required=0
                                :value="$page->translate($locale)->content"
                            />
                            <hr/>
                        @endforeach
                        <x-forms.submit value="update"/>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
