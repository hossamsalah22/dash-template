

@extends('dashboard.layouts.master')

@php($route = 'shops.items')

@section('title', __('translate.create') . ' ' . __('translate.' . Str::singular($route)))

@section('breadcrumbs', Breadcrumbs::render('shops.items.edit', $shop, $item->id))

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="row g-3 " action="{{ route("dashboard.{$route}.update", [$shop->id, $item->id]) }}"
                        method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        @foreach (config('translatable.locales') as $locale)
                            <h5 class=" mb-4">{{ __("translate.$locale.$locale") }}</h5>
                            <x-forms.input label='name' name="{{ $locale . '[name]' }}" type="text"
                                error_name="{{ $locale . '.name' }}" value="{{ $item->translate($locale)->name }}" />

                            {{-- <x-forms.editor label='description' name="{{ $locale . '[description]' }}"
                                error_name="{{ $locale . '.description' }}" value="{!! $item->translate($locale)->description !!}" /> --}}
                                <x-forms.textarea label="description" name="{{ $locale . '[description]' }}" type="text" error_name="{{ $locale . '.description' }}"  value="{{ $item->translate($locale)->description }}"/>

                                @endforeach
                        <x-forms.input label="price" name="price"  type="number" value="{{ $item->price }}" step="0.01" :min=0 />
                        <x-forms.uploadFile name="image" path="{{ $item->thumbnail }}" :required=false />
                        <x-forms.submit value="update" />
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
