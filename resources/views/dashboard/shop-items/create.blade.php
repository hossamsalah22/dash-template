
@extends('dashboard.layouts.master')

@php($route = 'shops.items')

@section('title', __('translate.create') . ' ' . __('translate.' . Str::singular($route)))

@section('breadcrumbs', Breadcrumbs::render('shops.items.create' , $shop))

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="row g-3 " action="{{ route("dashboard.{$route}.store", $shop->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @foreach (config('translatable.locales') as $locale)
                            <h5 class=" mb-4">{{ __("translate.$locale.$locale") }}</h5>
                            <x-forms.input label='name' name="{{ $locale . '[name]' }}" type="text"
                                error_name="{{ $locale . '.name' }}" />
                            {{-- <x-forms.editor label="description" name="{{ $locale . '[description]' }}"
                                error_name="{{ $locale . '.description' }}" /> --}}
                                <x-forms.textarea label="description" name="{{ $locale . '[description]' }}" type="text" error_name="{{ $locale . '.description' }}"  />

                        @endforeach
                        <x-forms.input label="price" name="price" type="number" min=0  step="0.01"/>
                        <x-forms.uploadFile name="image"  />
                        <x-forms.submit value="create" />
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
