@extends('dashboard.layouts.master')

@php($route='notifications')

@section('title', __('translate.'.$route))

@section('breadcrumbs', Breadcrumbs::render('resource'))

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}">
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title">@lang("translate.{$route}")</h4>

                    <table id="datatable" class="table table-bordered dt-responsive nowrap"
                           style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th> @lang('translate.title')</th>
                            <th> @lang('translate.message')</th>
                            <th> @lang('translate.type')</th>
                            <th> @lang('translate.link')</th>
                            <th> @lang('translate.read_at')</th>
                            <th> @lang('translate.read_by')</th>
                            <th> @lang('translate.created_at')</th>


                        </tr>
                        </thead>


                        <tbody>

                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@push('scripts')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/libs/datatables/datatables.min.js') }}"></script>

    <!-- Datatable init js -->
    {{-- <script src="{{URL::asset('assets/js/pages/datatables.init.js')}}"></script> --}}

    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
    <script>
        let datatableUrl = '{{ route("dashboard.{$route}.index") }}';
    </script>
    <script>
        $(document).ready(function() {

            let table = $('#datatable').DataTable({
                "language": {
                    "url": $('#importLangLocal').attr('data-LangLocal'),

                },
                serverSide: true,
                ajax: {
                    url: datatableUrl,
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false, orderable: false },
                    { data: 'title_text' },
                    { data: 'message_text' },
                    { data: 'type' },
                    { data: 'link' },
                    { data: 'read_at' },
                    { data: 'read_by' },
                    { data: 'created_at' },
                ],

            });
        });



    </script>
@endpush



