@extends('dashboard.layouts.master')

@php($route='contacts')

@section('title', __('translate.'.$route))

@section('breadcrumbs', Breadcrumbs::render('resource'))

@section('css')
    /*<!-- DataTables -->*/
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/libs/datatables/datatables.min.css') }}">
@endsection

{{--@section('action')--}}
{{--    <button class="btn btn-primary  dropdown-toggle" type="button"--}}
{{--            onclick="location.href='{{ route("dashboard.{$route}.create") }}'">--}}
{{--        <i class="mdi mdi-plus"></i> @lang('translate.create')--}}
{{--    </button>--}}
{{--@endsection--}}

{{--@include('components.datatable.delete')--}}
<x-datatable.delete :$route/>


@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">


                    <table id="datatable" class="table table-bordered dt-responsive nowrap"
                           style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th> #</th>
                            <th> @lang('translate.name')</th>
                            <th> @lang('translate.email')</th>
                            <th> @lang('translate.phone')</th>
                            <th> @lang('translate.subject')</th>
                            <th> @lang('translate.type')</th>
                            <th> @lang('translate.is_read')</th>
                            <th> @lang('translate.is_replied')</th>
                            <th> @lang('translate.created_at')</th>
                            <th> @lang('translate.actions')</th>

                        </tr>
                        </thead>


                        <tbody>

                        </tbody>
                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@push('scripts')
    <!-- Required datatable js -->
    <script src="{{ asset('assets/libs/datatables/datatables.min.js') }}"></script>

    <!-- Datatable init js -->
    {{-- <script src="{{URL::asset('assets/js/pages/datatables.init.js')}}"></script> --}}

    <script src="{{ URL::asset('assets/js/app.js') }}"></script>
    <script>
        let datatableUrl = '{{ route("dashboard.{$route}.index") }}';
    </script>
    <script>
        $(document).ready(function() {

            let table = $('#datatable').DataTable({
                "language": {
                    "url": $('#importLangLocal').attr('data-LangLocal'),

                },
                serverSide: true,
                ajax: {
                    url: datatableUrl,
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', searchable: false, orderable: false },

                    {
                        data: 'name',
                        name: 'name',
                        searchable: true,
                        sortable: true
                    },{
                        data: 'email',
                        name: 'email',
                        searchable: true,
                        sortable: true
                    },{
                        data: 'phone',
                        name: 'phone',
                        searchable: true,
                        sortable: true
                    },
                    {
                        data: 'subject',
                        name: 'subject',
                        searchable: true,
                        sortable: true
                    },{
                        data: 'type',
                        name: 'type',
                        searchable: true,
                        sortable: true
                    },


                    {
                        data: 'is_read',
                        name: 'is_read',
                    },
                    {
                        data: 'is_replied',
                        name: 'is_replied',
                    },

                    {
                        data: 'created_at',
                        name: 'created_at',
                        searchable: false
                    },
                    {
                        data: 'actions',
                        name: 'actions',
                        searchable: false,
                        orderable: false
                    }
                ],

            });
        });

        $('#datatable').on('change', '.toggle-is-active', function() {
            let recordId = $(this).data('id');
            let isActive = $(this).prop('checked');
            let property = $(this).data('property');
            let name = $(this).data('route-name')??'toggle';
            let value = isActive ? 1 : 0;
            const noty = new Noty({
                timeout: 2000,
            });
            $.ajax({
                url: "{{ route("dashboard.{$route}.read", ':recordId') }}".replace(':recordId', recordId).replace('read', name),
                type: "POST",
                data: {
                    property: property,
                    value: value,
                    _token: "{{ csrf_token() }}"
                },
                success: function(response) {
                    noty.push({
                        text: response.message,
                        type: 'success',
                    });

                },
                error: function(error) {
                    console.log(error);
                    noty.push({
                        text: error.responseJSON.message,
                        type: 'error',
                    });
                }
            });
        });

    </script>
@endpush



