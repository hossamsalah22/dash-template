@extends('dashboard.layouts.master')

@php($route='contacts')

@section('title', __('translate.show') ." "  . __('translate.'.Str::singular($route) ))

@section('breadcrumbs', Breadcrumbs::render('resource'))

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <table class="table">
                        <tbody>
                        @foreach($contact->toArray() as $key=>$value)
                            @if(in_array($key,['id','email_verified_at','created_at','updated_at','subject','subject_id']))
                                @continue
                            @endif
                            <tr class="row">
                                <td class="col-3">{{__("translate.".$key)}}</td>
                                <td  class="col-9">{{$value}}</td>
                            </tr>
                        @endforeach
                        <tr  class="row">
                            <td>
                                <form action="{{ route('dashboard.contacts.read',$contact->id) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-primary">
                                        @if($contact->is_read)
                                            <i class="mdi mdi-email-alert"></i> @lang('translate.mark_as_unread')
                                        @else
                                            <i class="mdi mdi-email-open"></i> @lang('translate.mark_as_read')
                                        @endif
                                    </button>
                                </form>
                            </td>
                            <td>
                                <form action="{{ route('dashboard.contacts.replied',$contact->id) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-primary">
                                        @if($contact->is_replied)
                                            <i class="mdi mdi-reply"></i> @lang('translate.mark_as_unreplied')
                                        @else
                                            <i class="mdi mdi-reply"></i> @lang('translate.mark_as_replied')
                                        @endif
                                    </button>
                                </form>
                            </td>

                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
