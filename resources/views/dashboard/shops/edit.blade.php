




@extends('dashboard.layouts.master')

@php($route = 'shops')

@section('title', __('translate.edit') . ' ' . __('translate.' . Str::singular($route)))

@section('breadcrumbs', Breadcrumbs::render('resource'))

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="row g-3 " action="{{ route("dashboard.{$route}.update", $shop->id) }}"
                        method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        @foreach (config('translatable.locales') as $locale)
                            <h5 class=" mb-4">{{ __("translate.$locale.$locale") }}</h5>
                            <x-forms.input label='name' name="{{ $locale . '[name]' }}" type="text"
                                error_name="{{ $locale . '.name' }}" value="{{ $shop->translate($locale)->name }}" />

                            {{-- <x-forms.editor label='description' name="{{ $locale . '[description]' }}"
                                error_name="{{ $locale . '.description' }}" value="{!! $shop->translate($locale)->description !!}" /> --}}
                                <x-forms.textarea label="description" name="{{ $locale . '[description]' }}" type="text" error_name="{{ $locale . '.description' }}"  :value="$shop->translate($locale)->description"/>

                                @endforeach
                        <x-forms.select label="shop_location" name="shop_location_id[]" :options=$locations  :multiple=true :value=$selectedLocations   placeholder="select_location" error_name="shop_location_id"  />
                        <x-forms.uploadFile name="image" path="{{$shop->thumbnail}}" :required=false />
                        <x-forms.submit value="update"/>
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
