@extends('dashboard.layouts.master')

@php($route='admins')

@section('title', __('translate.edit') .' ' . __('translate.'.Str::singular($route) ))

@section('breadcrumbs', Breadcrumbs::render('resource'))

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="row g-3" action="{{ route('dashboard.admins.update', $admin->id) }}"
                        method="post">
                        @csrf
                        @method('PUT')
                        <x-forms.input label="name" name="name" type="text" error_name="name"
                            value="{{ $admin->name }}" />
                        <x-forms.input label="email" name="email" type="email" error_name="email"
                            value="{{ $admin->email }} " />
                        <x-forms.input label="phone" name="phone" type="text" error_name="phone"
                            value="{{ $admin->phone }} " />
                        <x-forms.input label="password" name="password" type="password" :required=false
                            error_name="password" />
                        <x-forms.input label="password_confirmation" name="password_confirmation"
                            :required=false type="password" :required=false
                            error_name="password_confirmation" />
                        <x-forms.submit value="update" />
                    </form>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
