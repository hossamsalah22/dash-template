@extends('dashboard.layouts.master')

@php($route='admins')

@section('title', __('translate.create') .' ' . __('translate.'.Str::singular($route) ))

@section('breadcrumbs', Breadcrumbs::render('resource'))
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form class="row g-3 " action="{{route('dashboard.admins.store')}}" method="post" >
                        @csrf
                        <x-flash_message/>
                        <x-forms.input label="name" name="name" type="text"  error_name="name"/>
                        <x-forms.input label="email" name="email" type="email"  error_name="email"/>
                        <x-forms.input label="phone" name="phone" type="text"  error_name="phone"/>
                        <x-forms.input label="password" name="password" type="password"  error_name="password"/>
                        <x-forms.input label="password_confirmation" name="password_confirmation" type="password" :required=false error_name="password_confirmation"/>
                        <x-forms.submit value="create"/>
                    </form>


                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection
